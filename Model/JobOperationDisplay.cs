﻿
namespace Model
{
    public partial class JobOperationDisplay
    {
        public string JobID {get; set;}
        public int JobAssemblyID { get; set; }
        public int OperationID { get; set; }
        public decimal OperationType { get; set; }
        public string WorkCenterID { get; set; }
        public string ProcessID { get; set; }
        public string ProcessShortDescription { get; set; }
        public decimal SetupComplete { get; set; }
        public decimal ProductionComplete { get; set; }
        public decimal WorkCenterMachineID { get; set; }
        public string PartID { get; set; }
        public string PartRevisionID { get; set; }

    }
}
