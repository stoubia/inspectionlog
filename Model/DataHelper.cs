﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using EF_M1;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading;
using System.Data.Entity.Core;
using System.Data.Entity.Validation;
using System.Data.SqlClient;

namespace Model
{
    public class DataHelper
    {
        private bool debugging = false;

        private const int MAX_RETRY = 1;
        private const double LONG_WAIT_SECONDS = 2;
        private const double SHORT_WAIT_SECONDS = 0.5;
        private static readonly TimeSpan longWait = TimeSpan.FromSeconds(LONG_WAIT_SECONDS);
        private static readonly TimeSpan shortWait = TimeSpan.FromSeconds(SHORT_WAIT_SECONDS);
        private enum RetryableSqlErrors
        {
            ServerNotFound = -1,
            Timeout = -2,
            NoLock = 1204,
            Deadlock = 1205,
            WordbreakerTimeout = 30053,
        }

        //not used
        private void Retry<T>(Action<T> retryAction) where T : M1Context, new()
        {
            var retryCount = 0;
            using (var ctx = new T())
            {
                for (; ; )
                {
                    try
                    {
                        retryAction(ctx);
                        break;
                    }
                    catch (SqlException ex)
                    {
                        if (!Enum.IsDefined(typeof(RetryableSqlErrors), ex.Number))
                            throw;

                        retryCount++;
                        if (retryCount > MAX_RETRY) throw;

                        Thread.Sleep(ex.Number == (int)RetryableSqlErrors.Timeout ?
                                                                longWait : shortWait);
                    }
                }
            }
        }

        //not used
        public async Task<List<QualityRegisterQueue>> GetQualityRegisterAsync2(M1Context m1)
        {
            return await (from a in m1.QualityRegisters
                            where (a.qanClosed == 0)
                            //orderby a.qanAssignedDate descending, a.qanOpenedDate
                            orderby a.qanAssignedDate.HasValue descending, a.qanAssignedDate, a.qanOpenedDate
                            select new QualityRegisterQueue
                            {
                                QualityRegisterID = a.qanQualityRegisterID,
                                JobID = a.qanJobID.Trim(),
                                JobAssemblyID = a.qanJobAssemblyID,
                                JobOperationID = a.qanJobOperationID,
                                PartID = a.qanPartID.Trim(),
                                PartRevisionID = a.qanPartRevisionID.Trim(),
                                PartShortDescription = a.qanPartShortDescription.Trim(),
                                OpenedByEmployeeID = a.qanOpenedByEmployeeID.Trim(),
                                OpenedByEmployeeName = a.OpenedEmployee.lmeEmployeeName.Trim(),
                                OpenedDate = a.qanOpenedDate,
                                PartImage = a.JobAssembly.ujmaPartImage,
                                AssignedDate = a.qanAssignedDate,
                                AssignedToEmployeeID = a.qanAssignedToEmployeeID.Trim(),
                                AssignedToEmployeeName = a.AssignedEmployee.lmeEmployeeName.Trim()
                            }).ToListAsync();
        }

        public async Task<List<QualityRegisterQueue>> GetQualityRegisterAsync()
        {
           if(debugging) await Task.Delay(1000);
           var retryCount = 0;
           using (M1Context m1 = new M1Context())
           {
               for (; ; )
               {
                   try
                   {
                       return await (from a in m1.QualityRegisters
                                     where (a.qanClosed == 0)
                                     && a.qanSource == 5 && a.qanQualityRegisterType == 3
                                     orderby a.qanAssignedDate.HasValue descending, a.qanAssignedDate, a.qanOpenedDate
                                     select new QualityRegisterQueue
                                     {
                                         QualityRegisterID = a.qanQualityRegisterID,
                                         JobID = a.qanJobID.Trim(),
                                         JobAssemblyID = a.qanJobAssemblyID,
                                         JobOperationID = a.qanJobOperationID,
                                         PartID = a.qanPartID.Trim(),
                                         PartRevisionID = a.qanPartRevisionID.Trim(),
                                         PartShortDescription = a.qanPartShortDescription.Trim(),
                                         OpenedByEmployeeID = a.qanOpenedByEmployeeID.Trim(),
                                         OpenedByEmployeeName = a.OpenedEmployee.lmeEmployeeName.Trim(),
                                         OpenedDate = a.qanOpenedDate,
                                         PartImage = a.JobAssembly.ujmaPartImage,
                                         AssignedDate = a.qanAssignedDate,
                                         AssignedToEmployeeID = a.qanAssignedToEmployeeID.Trim(),
                                         AssignedToEmployeeName = a.AssignedEmployee.lmeEmployeeName.Trim()
                                     }).ToListAsync().ConfigureAwait(false);
                   }
                   catch (SqlException ex)
                   {
                       Debug.WriteLine("SQL Exception number = " + ex.Number);
                       if (!Enum.IsDefined(typeof(RetryableSqlErrors), ex.Number))
                           throw new M1Exception(ex.Message, ex);

                       retryCount++;

                       if (retryCount > MAX_RETRY) throw new M1Exception(ex.Message, ex); ;
                        Debug.WriteLine("Retrying. Count = " + retryCount);
                       Thread.Sleep(ex.Number == (int)RetryableSqlErrors.Timeout ?
                                                               longWait : shortWait);
                   }
                   catch (EntityException ex)
                   {
                       Debug.WriteLine("EntityException  = " + ex.Message);


                       throw new M1Exception(ex.Message, ex);


                   }
               }
           }  
        }

        #region old GetQualityRegister
        //try
        //{
        //    if (debugging) Debug.WriteLine("Starting Using");
        //    using (M1Context m1 = new M1Context())
        //    {
        //        if (debugging) Debug.WriteLine("Started Using");
        //        return await (from a in m1.QualityRegisters
        //                 where (a.qanClosed == 0)
        //                 //orderby a.qanAssignedDate descending, a.qanOpenedDate
        //                 orderby a.qanAssignedDate.HasValue descending, a.qanAssignedDate, a.qanOpenedDate
        //                 select new QualityRegisterQueue
        //                 {
        //                     QualityRegisterID = a.qanQualityRegisterID,
        //                     JobID = a.qanJobID.Trim(),
        //                     JobAssemblyID = a.qanJobAssemblyID,
        //                     JobOperationID = a.qanJobOperationID,
        //                     PartID = a.qanPartID.Trim(),
        //                     PartRevisionID = a.qanPartRevisionID.Trim(),
        //                     PartShortDescription = a.qanPartShortDescription.Trim(),
        //                     OpenedByEmployeeID = a.qanOpenedByEmployeeID.Trim(),
        //                     OpenedByEmployeeName = a.OpenedEmployee.lmeEmployeeName.Trim(),
        //                     OpenedDate = a.qanOpenedDate,
        //                     PartImage = a.JobAssembly.ujmaPartImage,
        //                     AssignedDate = a.qanAssignedDate,
        //                     AssignedToEmployeeID = a.qanAssignedToEmployeeID.Trim(),
        //                     AssignedToEmployeeName = a.AssignedEmployee.lmeEmployeeName.Trim()
        //                 }).ToListAsync();

        //    }
        //}
        //catch (EntityException ex)
        //{
        //    Debug.WriteLine("EntityException thrown");
        //    throw new M1Exception(ex.Message, ex);

        //}
        //catch (DbEntityValidationException e)
        //{
        //    Debug.WriteLine("DbEntityValidationException thrown");
        //    #region DebugOutput
        //    foreach (var eve in e.EntityValidationErrors)
        //    {
        //        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
        //            eve.Entry.Entity.GetType().Name, eve.Entry.State);
        //        foreach (var ve in eve.ValidationErrors)
        //        {
        //            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
        //                ve.PropertyName, ve.ErrorMessage);
        //        }

        //    }
        //    #endregion

        //    throw ;
        //}
        //catch (SqlException ex)
        //{
        //    Debug.WriteLine("SqlException thrown");
        //    throw new M1Exception(ex.Message, ex);
        //}
#endregion

        /// <summary>
        /// Gets an employee list for active inspectors
        /// </summary>
        /// <returns>A List of EmployeeDisplay</returns>
        public async Task<List<EmployeeDisplay>> GetInspectorsAsync()
        {
            if (debugging) await Task.Delay(2000);
            var retryCount = 0;
            using (M1Context m1 = new M1Context())
            {
                for (;;)
                {
                    try
                    {
                        return await (from a in m1.Employees
                                      where (a.lmeTerminationDate == null)   //Employee is active
                                      && (a.lmeInspectorEmployee == -1)      //Employee is flagged as an inspector
                                      orderby a.lmeEmployeeName              //Order results alphabetically
                                      select new EmployeeDisplay
                                      {
                                          EmployeeID = a.lmeEmployeeID,
                                          EmployeeName = a.lmeEmployeeName.ToUpper().Trim()
                                      }).ToListAsync().ConfigureAwait(false);
                    }
                    catch (SqlException ex)
                    {
                        Debug.WriteLine("SQL Exception number = " + ex.Number);
                        if (!Enum.IsDefined(typeof(RetryableSqlErrors), ex.Number))
                            throw new M1Exception(ex.Message, ex);

                        retryCount++;

                        if (retryCount > MAX_RETRY) throw new M1Exception(ex.Message, ex); ;
                        Debug.WriteLine("Retrying. Count = " + retryCount);
                        Thread.Sleep(ex.Number == (int)RetryableSqlErrors.Timeout ?
                                                                longWait : shortWait);
                    }
                    catch (EntityException ex)
                    {
                        Debug.WriteLine("EntityException  = " + ex.Message);

                        throw new M1Exception(ex.Message, ex);

                    }
                    
                }
            }
        }

        /// <summary>
        /// Gets the scrap reasons for Failed Inspections.
        /// </summary>
        /// <returns>List of Reasons</returns>
        public async Task<List<Reason>> GetScrapReasonsAsync()
        {
            if (debugging) await Task.Delay(2000);
            var retryCount = 0;

            using (M1Context m1 = new M1Context())
            {
                for (; ; )
                {
                    try
                    {
                        return await (from a in m1.Reasons
                                      where a.xarReasonType.Trim().Equals("S")   //S is the category fro Scrap Reasons 
                                      select a).ToListAsync();
                    }
                    catch (SqlException ex)
                    {
                        Debug.WriteLine("SQL Exception number = " + ex.Number);
                        if (!Enum.IsDefined(typeof(RetryableSqlErrors), ex.Number))
                            throw new M1Exception(ex.Message, ex);

                        retryCount++;

                        if (retryCount > MAX_RETRY) throw new M1Exception(ex.Message, ex); ;
                        Debug.WriteLine("Retrying. Count = " + retryCount);
                        Thread.Sleep(ex.Number == (int)RetryableSqlErrors.Timeout ?
                                                                longWait : shortWait);
                    }
                    catch (EntityException ex)
                    {
                        Debug.WriteLine("EntityException  = " + ex.Message);

                        throw new M1Exception(ex.Message, ex);

                    }
                }

                
            }

        }

        /// <summary>
        /// Gets a single Image for the selected JobAssembly
        /// </summary>
        /// <param name="job"></param>
        /// <param name="jobAssy"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        public async Task<byte[]> GetJobImagesAsync(string job, int jobAssy, CancellationToken ct)
        {
            if (debugging) await Task.Delay(5000);
            var retryCount = 0;
            using (M1Context m1 = new M1Context())
            {
                for (; ; )
                {
                    try
                    {
                        return await (from a in m1.JobAssemblies
                                      where (a.jmaJobID.Equals(job.Trim()))      //=JobID
                                      && (a.jmaJobAssemblyID.Equals(jobAssy))    //=JobAssemblyID
                                      select a.ujmaPartImage).SingleOrDefaultAsync(ct);
                    }
                    catch (SqlException ex)
                    {
                        Debug.WriteLine("SQL Exception number = " + ex.Number);
                        if (!Enum.IsDefined(typeof(RetryableSqlErrors), ex.Number))
                            throw new M1Exception(ex.Message, ex);

                        retryCount++;

                        if (retryCount > MAX_RETRY) throw new M1Exception(ex.Message, ex); ;
                        Debug.WriteLine("Retrying. Count = " + retryCount);
                        Thread.Sleep(ex.Number == (int)RetryableSqlErrors.Timeout ?
                                                                longWait : shortWait);
                    }
                    catch (EntityException ex)
                    {
                        Debug.WriteLine("EntityException  = " + ex.Message);


                        throw new M1Exception(ex.Message, ex);


                    }

                }
            }
        }

        /// <summary>
        /// Gets a list of operations for the specified Job
        /// </summary>
        /// <param name="job"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        public async Task<List<JobOperationDisplay>> GetOperations(JobView job, CancellationToken ct)
        {
            if (debugging) await Task.Delay(5000);
            var retryCount = 0;
            using (M1Context m1 = new M1Context())
            {
                for (; ; )
                {
                    try
                    {
                        return await (from a in m1.JobOperations
                                      where (a.jmoJobID == job.JobID)                //=JobID
                                      && (a.jmoJobAssemblyID == job.JobAssemblyID)   //=JobAssemblyID
                                      && (a.jmoProductionComplete == 0)              //Operation is NOT production complete
                                      orderby a.jmoJobOperationID                    //Order by OperationID ascending
                                      select new JobOperationDisplay
                                      {
                                          JobID = a.jmoJobID.Trim(),
                                          JobAssemblyID = a.jmoJobAssemblyID,
                                          OperationID = a.jmoJobOperationID,
                                          ProcessID = a.jmoProcessID.Trim(),
                                          ProcessShortDescription = a.jmoProcessShortDescription.Trim()
                                      }).ToListAsync(ct);
                    }
                    catch (SqlException ex)
                    {
                        Debug.WriteLine("SQL Exception number = " + ex.Number);
                        if (!Enum.IsDefined(typeof(RetryableSqlErrors), ex.Number))
                            throw new M1Exception(ex.Message, ex);

                        retryCount++;

                        if (retryCount > MAX_RETRY) throw new M1Exception(ex.Message, ex); ;
                        Debug.WriteLine("Retrying. Count = " + retryCount);
                        Thread.Sleep(ex.Number == (int)RetryableSqlErrors.Timeout ?
                                                                longWait : shortWait);
                    }
                    catch (EntityException ex)
                    {
                        Debug.WriteLine("EntityException  = " + ex.Message);


                        throw new M1Exception(ex.Message, ex);


                    }

                }
            }
        }

        /// <summary>
        /// Gets all active employees flagged as Shop Employees in the Employees table.
        /// </summary>
        /// <returns></returns>
        public async Task<List<EmployeeDisplay>> GetShopEmployees()
        {
            if (debugging) await Task.Delay(5000);
            var retryCount = 0;
            using (M1Context m1 = new M1Context())
            {
                for (; ; )
                {
                    try
                    {
                        return await (from a in m1.Employees
                                      where (a.lmeTerminationDate == null)   //employee is active
                                      && (a.lmeShopEmployee == -1)           //employee is a shop employee
                                      orderby a.lmeEmployeeName              //order alphabetically
                                      select new EmployeeDisplay
                                      {
                                          EmployeeID = a.lmeEmployeeID,
                                          EmployeeName = a.lmeEmployeeName.ToUpper().Trim()
                                      }).ToListAsync();
                    }
                    catch (SqlException ex)
                    {
                        Debug.WriteLine("SQL Exception number = " + ex.Number);
                        if (!Enum.IsDefined(typeof(RetryableSqlErrors), ex.Number))
                            throw new M1Exception(ex.Message, ex);

                        retryCount++;

                        if (retryCount > MAX_RETRY) throw new M1Exception(ex.Message, ex); ;
                        Debug.WriteLine("Retrying. Count = " + retryCount);
                        Thread.Sleep(ex.Number == (int)RetryableSqlErrors.Timeout ?
                                                                longWait : shortWait);
                    }
                    catch (EntityException ex)
                    {
                        Debug.WriteLine("EntityException  = " + ex.Message);


                        throw new M1Exception(ex.Message, ex);


                    }

                }
            }

        }

        /// <summary>
        /// Gets a list of all open and in-production jobs where the JobID starts with the jobID parameter
        /// </summary>
        /// <param name="job"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        public async Task<List<JobView>> GetJobsAsync(string jobID, CancellationToken ct)
        {
            if (debugging) await Task.Delay(5000);
            var retryCount = 0;
            using (M1Context m1 = new M1Context())
            {
                for (; ; )
                {
                    try
                    {
                        return await (from a in m1.JobAssemblies
                                      where (a.jmaProductionComplete == 0)
                                      && (a.jmaClosed == 0)
                                      && (a.Job.jmpClosed == 0)
                                      && (a.Job.jmpProductionComplete == 0)
                                      && (a.jmaJobID.StartsWith(jobID))
                                      orderby a.Job.jmpProductionDueDate     //order by due date ascending
                                      select new JobView
                                      {
                                          JobID = a.jmaJobID.Trim(),
                                          JobAssemblyID = a.jmaJobAssemblyID,
                                          PartID = a.jmaPartID.Trim(),
                                          Customer = a.Job.jmpCustomerOrganizationID.Trim(),
                                          PartRevisionID = a.jmaPartRevisionID.Trim(),
                                          PartShortDescription = a.jmaPartShortDescription
                                      }).ToListAsync(ct);
                    }
                    catch (SqlException ex)
                    {
                        Debug.WriteLine("SQL Exception number = " + ex.Number);
                        if (!Enum.IsDefined(typeof(RetryableSqlErrors), ex.Number))
                            throw new M1Exception(ex.Message, ex);

                        retryCount++;

                        if (retryCount > MAX_RETRY) throw new M1Exception(ex.Message, ex); ;
                        Debug.WriteLine("Retrying. Count = " + retryCount);
                        Thread.Sleep(ex.Number == (int)RetryableSqlErrors.Timeout ?
                                                                longWait : shortWait);
                    }
                    catch (EntityException ex)
                    {
                        Debug.WriteLine("EntityException  = " + ex.Message);


                        throw new M1Exception(ex.Message, ex);


                    }

                }

            }
        }

        /// <summary>
        /// Will Insert an Inspection and InspectionLine record into M1 and update the existing QualityRegister record.
        /// User will select one of the items in the QualityRegister Log, select their name from the inspectors list,
        /// and press the start button, signifying the begining of an inspection.
        /// </summary>
        /// <param name="inspector"></param>
        /// <param name="register"></param>
        /// <returns></returns>
        public async Task CreateInspection(EmployeeDisplay inspector, QualityRegisterQueue register)
        {
            if (debugging) await Task.Delay(5000);
            var retryCount = 0;
            using (M1Context m1 = new M1Context())
            {
                for (; ; )
                {
                    try
                    {
                        var now = DateTime.Now; //So all dates end up the same?  I think?

                        ////create new Inspection and InspectionLine records
                        var inspection = new Inspection();
                        var inspectionLine = new InspectionLine();

                        //Get the NextID record for the Inspections table
                        NextID nextID = (from a in m1.NextIDs where a.xanTable == "INSPECTIONS" select a).SingleOrDefault();

                        //Assign the NextID for both the Inspection and InspectionLine record
                        inspection.qapInspectionID = nextID.xanNextID.Trim();
                        inspectionLine.qalInspectionID = inspection.qapInspectionID;

                        //increment the NextID by 1
                        nextID.xanNextID = (int.Parse(nextID.xanNextID.Trim()) + 1).ToString();

                        //Assign the values
                        inspection.qapInspectorEmployeeID = inspector.EmployeeID;   //set the Inspector Name
                        inspection.qapInspectionDate = now;
                        inspection.qapCreatedDate = now;
                        inspectionLine.qalPartID = register.PartID;
                        inspectionLine.qalPartRevisionID = register.PartRevisionID;
                        inspectionLine.qalPartShortDescription = register.PartShortDescription;
                        inspectionLine.qalCreatedDate = now;
                        inspectionLine.qalInspectionLineID = 1;     //M1 is structured to allow multiple lines per inspection, but InspectionLog will only do 1:1
                        inspectionLine.qalQuantityToInspect = 1;    //for now we will always work with the assumption that we are only inspecting 1 part.
                        inspectionLine.qalFirstOffInspection = register.FirstOffInspection;
                        inspectionLine.qalQualityRegisterID = register.QualityRegisterID;

                        //Get the QualityRegister record that needs to be updated.
                        QualityRegister QR = (from b in m1.QualityRegisters where b.qanQualityRegisterID == register.QualityRegisterID select b).SingleOrDefault();
                        QR.qanAssignedToEmployeeID = inspector.EmployeeID;  //update the AssignedToEmployee (the inspector)
                        QR.qanAssignedDate = now;                           //update the AssignedDate
                        QR.qanStatus = 1;                                   // status: none -> inspecting

                        //Add the Inspection and InspectionLine record to the M1 Context
                        m1.Inspections.Add(inspection);
                        m1.InspectionLines.Add(inspectionLine);

                        //Save changes to the Database
                        await m1.SaveChangesAsync();
                        Debug.WriteLine("Done awaiting");
                        return;
                    }
                    catch (SqlException ex)
                    {
                        Debug.WriteLine("SQL Exception number = " + ex.Number);
                        if (!Enum.IsDefined(typeof(RetryableSqlErrors), ex.Number))
                            throw new M1Exception(ex.Message, ex);

                        retryCount++;

                        if (retryCount > MAX_RETRY) throw new M1Exception(ex.Message, ex); ;
                        Debug.WriteLine("Retrying. Count = " + retryCount);
                        Thread.Sleep(ex.Number == (int)RetryableSqlErrors.Timeout ?
                                                                longWait : shortWait);
                    }
                    catch (EntityException ex)
                    {
                        Debug.WriteLine("EntityException  = " + ex.Message);


                        throw new M1Exception(ex.Message, ex);


                    }

                }
            }

        }

        /// <summary>
        /// Will Insert a QualityRegister record into M1.  The input parameters are selected from the 
        /// </summary>
        /// <param name="job"></param>
        /// <param name="operation"></param>
        /// <param name="employee"></param>
        /// <param name="setup"></param>
        /// <returns>Returns a QualityRegisterQueue to be passed back to the Log List</returns>
        public async Task<QualityRegisterQueue> InsertQualityRegister(JobView job, JobOperationDisplay operation, EmployeeDisplay employee, bool setup)
        {
            if (debugging) await Task.Delay(5000);
            var retryCount = 0;
            using (M1Context m1 = new M1Context())
            {
                for (; ; )
                {
                    try
                    {
                        //TODO: M1 actually reserves the NextID when the user starts to create a new record.  If the user cancels new record creation, M1 reverts the NextID back to the orgiginal.
                        //      This would prevent NextID conflicts when multiple users are creating records simultaniously and will be necessary if we are running multiple applicaitons.
                        //      In this scenario the delay between getting the NextID and writing the new record is very small because all the data is collected before hand,
                        //      but this is still a potential problem that probably at the very least needs to be caught with a retry.


                        //Create new QualityRegister
                        var x = new QualityRegister();

                        //Get the current NextID record for QualityRegisters
                        NextID nextID = (from a in m1.NextIDs where a.xanTable == "QualityRegisters" select a).SingleOrDefault();

                        //Set the new QualityRegister record ID
                        x.qanQualityRegisterID = nextID.xanNextID.Trim();

                        //Increment the NextID
                        nextID.xanNextID = (int.Parse(nextID.xanNextID.Trim()) + 1).ToString();


                        x.qanCustomerOrganizationID = job.Customer;
                        x.qanPartID = job.PartID;
                        x.qanPartRevisionID = job.PartRevisionID;
                        x.qanPartShortDescription = job.PartShortDescription;
                        x.qanRegisterQuantity = 1;                      //is required in M1, we are not doing anything with quantities but we will assume that everything is in quantities of 1 (assuming setup inspection
                        x.qanOpenedByEmployeeID = employee.EmployeeID;  //The person submitting the part to inspection
                        x.qanOpenedDate = DateTime.Now;                 //Date and Time of submission
                        x.qanJobID = job.JobID;                         //JobID of the part being submitted
                        x.qanJobAssemblyID = job.JobAssemblyID;         //JobAssemblyID of the part being submitted
                        x.qanJobOperationID = operation.OperationID;    //OperatoinID of the part being submitted
                        x.qanSource = 5;                                //index for Job Source I think.  Should figure out how to make an additional source to differentiate between SFE and InspectionLog
                        x.qanCreatedBy = "InspLog";
                        x.qanCreatedDate = x.qanOpenedDate;             //Set the CreatedDate as the OpenedDate.  Maybe there is a way to make this DB generated
                        x.qanQualityRegisterType = 3;                   //Means it is a "Job" type of QualityRegister

                        //also known as First Off Inspection in M1
                        if (setup == true)
                        {
                            x.qanFirstOffInspection = -1;
                        }
                        else
                        {
                            x.qanFirstOffInspection = 0;
                        }

                        //Add the new QualityRegister record to the M1 Context
                        m1.QualityRegisters.Add(x);

                        //Call and wait for the changes to save
                        await m1.SaveChangesAsync();

                        return new QualityRegisterQueue(x, employee, job);
                    }
                    catch (SqlException ex)
                    {
                        Debug.WriteLine("SQL Exception number = " + ex.Number);
                        if (!Enum.IsDefined(typeof(RetryableSqlErrors), ex.Number))
                            throw new M1Exception(ex.Message, ex);

                        retryCount++;

                        if (retryCount > MAX_RETRY) throw new M1Exception(ex.Message, ex); ;
                        Debug.WriteLine("Retrying. Count = " + retryCount);
                        Thread.Sleep(ex.Number == (int)RetryableSqlErrors.Timeout ?
                                                                longWait : shortWait);
                    }
                    catch (EntityException ex)
                    {
                        Debug.WriteLine("EntityException  = " + ex.Message);


                        throw new M1Exception(ex.Message, ex);


                    }

                }
            }
        }

        /// <summary>
        /// Used to change the the inspector.  Updates QualityRegister and Inspection.  
        /// The two tables are linked by InspectionLines, so need to get the inspectionLine 
        /// record in order to get the InspectionID ('need' is subjective... it was an option)
        /// </summary>
        /// <param name="inspector"></param>
        /// <param name="register"></param>
        /// <returns></returns>
        public async Task<QualityRegisterQueue> UpdateQualityRegister(EmployeeDisplay inspector, QualityRegisterQueue register)
        {
            if (debugging) await Task.Delay(2000);
            var retryCount = 0;
            using (M1Context m1 = new M1Context())
            {
                for (; ; )
                {
                    try
                    {
                        //Get the QualityRegister with the same ID as the QualityRegisterQueue that was selected
                        QualityRegister QR = (from a in m1.QualityRegisters where a.qanQualityRegisterID == register.QualityRegisterID select a).SingleOrDefault();

                        //Get the InspectionLine that has the same QualityRegisterID as the QualityRegisterQueue.  This could be dangerous because I don't think theres a 1:1 requirement in M1 so it is possible to have 1:many which would throw an exception
                        InspectionLine InspLine = (from c in m1.InspectionLines where c.qalQualityRegisterID.Trim() == register.QualityRegisterID.Trim() select c).SingleOrDefault();

                        //Get the Inspection that has the same ID as the InspectionLine we got above.
                        
                        Inspection Insp = (from b in m1.Inspections where b.qapInspectionID == InspLine.qalInspectionID select b).SingleOrDefault();

                        //Add the updated items to the M1 Context
                        QR.qanAssignedToEmployeeID = inspector.EmployeeID;
                        Insp.qapInspectorEmployeeID = inspector.EmployeeID;
                        
                        //Save
                        await m1.SaveChangesAsync();
                        register.AssignedToEmployeeID = inspector.EmployeeID;
                        register.AssignedToEmployeeName = inspector.EmployeeName;
                        return register;
                    }
                    catch (SqlException ex)
                    {
                        Debug.WriteLine("SQL Exception number = " + ex.Number);
                        if (!Enum.IsDefined(typeof(RetryableSqlErrors), ex.Number))
                            throw new M1Exception(ex.Message, ex);

                        retryCount++;

                        if (retryCount > MAX_RETRY) throw new M1Exception(ex.Message, ex); ;
                        Debug.WriteLine("Retrying. Count = " + retryCount);
                        Thread.Sleep(ex.Number == (int)RetryableSqlErrors.Timeout ?
                                                                longWait : shortWait);
                    }
                    catch (EntityException ex)
                    {
                        Debug.WriteLine("EntityException  = " + ex.Message);


                        throw new M1Exception(ex.Message, ex);


                    }

                }
            }
        }   

        /// <summary>
        /// Updates QualityRegister, Inspection and InspectionLine to end the inspection and set all these records to closed.
        /// </summary>
        /// <param name="register"></param>
        /// <param name="scrap"></param>
        /// <returns>Returns the QualityRegisterID that has been closed</returns>
        public async Task<string> EndInspectionAsync(QualityRegisterQueue register, Reason scrap)
        {
            if (debugging) await Task.Delay(5000);
            var retryCount = 0;
            using (M1Context m1 = new M1Context())
            {
                for (; ; )
                {
                    try
                    {
                        var now = DateTime.Now;

                        QualityRegister QR = (from a in m1.QualityRegisters where a.qanQualityRegisterID == register.QualityRegisterID select a).SingleOrDefault();
                        InspectionLine InspLine = (from c in m1.InspectionLines where c.qalQualityRegisterID.Trim() == register.QualityRegisterID.Trim() select c).SingleOrDefault();
                        Inspection Insp = (from b in m1.Inspections where b.qapInspectionID == InspLine.qalInspectionID select b).SingleOrDefault();

                        //Closing Logic
                        QR.qanClosedByEmployeeID = QR.qanAssignedToEmployeeID;
                        QR.qanClosedDate = now;
                        QR.qanClosed = -1;
                        QR.qanStatus = 2;                   //status: inspecting -> inspection complete

                        Insp.qapClosedDate = now;
                        Insp.qapStatus = "C";               //Inspection status: Closed
                        if (scrap != null) InspLine.qalScrapReasonID = scrap.xarReasonID;
                        InspLine.qalInspectionComplete = -1;

                        await m1.SaveChangesAsync();
                        return QR.qanQualityRegisterID;  //ID of the item to be removed from list
                    }
                    catch (SqlException ex)
                    {
                        Debug.WriteLine("SQL Exception number = " + ex.Number);
                        if (!Enum.IsDefined(typeof(RetryableSqlErrors), ex.Number))
                            throw new M1Exception(ex.Message, ex);

                        retryCount++;

                        if (retryCount > MAX_RETRY) throw new M1Exception(ex.Message, ex); ;
                        Debug.WriteLine("Retrying. Count = " + retryCount);
                        Thread.Sleep(ex.Number == (int)RetryableSqlErrors.Timeout ?
                                                                longWait : shortWait);
                    }
                    catch (EntityException ex)
                    {
                        Debug.WriteLine("EntityException  = " + ex.Message);


                        throw new M1Exception(ex.Message, ex);


                    }

                }

            }
        }

    }
}
