﻿
namespace Model
{
    public partial class JobView
    {
        public string JobID { get; set; }
        public string Customer { get; set; }
        public int JobAssemblyID { get; set; }
        public string PartID { get; set; }
        public string PartRevisionID { get; set; }
        public string PartShortDescription { get; set; }
        public byte[] PartImage { get; set; }
    }
}
