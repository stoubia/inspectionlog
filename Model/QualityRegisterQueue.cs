﻿
using EF_M1;
using System;
namespace Model
{
    public partial class QualityRegisterQueue
    {
        public QualityRegisterQueue() { }

        public QualityRegisterQueue(QualityRegister qr)
        {
            QualityRegisterID = qr.qanQualityRegisterID;
            JobID = qr.qanJobID.Trim();
            JobAssemblyID = qr.qanJobAssemblyID;
            JobOperationID = qr.qanJobOperationID;
            PartID = qr.qanPartID.Trim();
            PartRevisionID = qr.qanPartRevisionID.Trim();
            PartShortDescription = qr.qanPartShortDescription.Trim();
            OpenedByEmployeeID = qr.qanOpenedByEmployeeID.Trim();
            OpenedByEmployeeName = qr.OpenedEmployee.lmeEmployeeName.Trim();
            OpenedDate = qr.qanOpenedDate;
            PartImage = qr.JobAssembly.ujmaPartImage;
            AssignedDate = qr.qanAssignedDate;
            AssignedToEmployeeID = qr.qanAssignedToEmployeeID.Trim();
            AssignedToEmployeeName = qr.AssignedEmployee.lmeEmployeeName.Trim();
        }
        /// <summary>
        /// Converts a QualityRegister member to a QualityRegisterQueue member.  
        /// This is to allow passing the newly created QualityRegister record directly to the
        /// Log instead of forcing the Log to query the DB for the newly added record. 
        /// </summary>
        /// <param name="qr">The QualityRegister record that has just been inserted to the DB</param>
        /// <param name="openedEmployee">Used to get the EmployeeName property (as opposed to EmployeeID)</param>
        /// <param name="job">Used only to get the Part Image</param>
        public QualityRegisterQueue(QualityRegister qr, EmployeeDisplay openedEmployee, JobView job)
        {
            QualityRegisterID = qr.qanQualityRegisterID;
            JobID = qr.qanJobID;
            JobAssemblyID = qr.qanJobAssemblyID;
            JobOperationID = qr.qanJobOperationID;
            PartID = qr.qanPartID;
            PartRevisionID = qr.qanPartRevisionID;
            PartShortDescription = qr.qanPartShortDescription;
            OpenedByEmployeeID = qr.qanOpenedByEmployeeID;
            OpenedByEmployeeName = qr.qanOpenedByEmployeeID == openedEmployee.EmployeeID ? openedEmployee.EmployeeName : "";
            OpenedDate = qr.qanOpenedDate;
            PartImage = job.PartImage;
            FirstOffInspection = qr.qanFirstOffInspection;
        }
        
        public string QualityRegisterID { get; set; }
        public string JobID { get; set; }
        public int JobAssemblyID { get; set; }
        public int JobOperationID { get; set; }
        public string  PartID { get; set; }
        public string  PartRevisionID { get; set; }
        public string PartShortDescription { get; set; }
        public string OpenedByEmployeeID { get; set; }
        public string OpenedByEmployeeName { get; set; }
        public DateTime? OpenedDate { get; set; }
        public byte[]  PartImage { get; set; }
        public decimal FirstOffInspection { get; set; }
        public DateTime? AssignedDate { get; set; }
        public string AssignedToEmployeeID { get; set; }
        public string AssignedToEmployeeName { get; set; }
    }
}
