﻿using GalaSoft.MvvmLight;
using System.Linq;
using Model;
using EF_M1;
using System.Collections.Generic;
using GalaSoft.MvvmLight.CommandWpf;
using System.Windows.Input;
using GalaSoft.MvvmLight.Messaging;
using InspectionApp.Messages;
using System.Diagnostics;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using Logger;

namespace InspectionApp.ViewModel
{
    public class AddQualityRegisterViewModel : ViewModelBase
    {
        DataHelper data = new DataHelper();
        CancellationTokenSource cts;

        public ICommand ExitAddQualityRegisterCommand { get; private set; }
        
        public ICommand ExitCommand { get; set; }
        public ICommand ClearSelectedJobCommand { get; set; }
        public ICommand ClearSelectedOperationCommand { get; set; }
        public ICommand ClearSelectedEmployeeCommand { get; set; }
        public ICommand AddQualityRegisterCommand { get; set; }
        public AddQualityRegisterSM AddQualityRegisterSM { get; set; }

        private JobView _selectedJob;
        private byte[] _selectedJobImage;
        private JobOperationDisplay _selectedOperation;
        private EmployeeDisplay _selectedEmployee;
        private List<JobView> _openJobsQueryResults;
        private List<JobView> _openJobsToDisplay;
        private List<JobOperationDisplay> _operations;
        private List<JobOperationDisplay> _operationsToDisplay;
        private List<EmployeeDisplay> _employeeQueryResults;
        private List<EmployeeDisplay> _employeesToDisplay;
        private string _jobToBeSearched;
        private string _operationToBeSearched;
        private string _employeeToBeSearched;
        private string _inputBox;
        private string _userDialog;
        private string _adornerText;
        private bool _setupInspectionYes;
        private bool _setupInspectionNo;
        private bool refreshLogFlag;
        //private bool waitingForImage;
        
        public AddQualityRegisterViewModel()
        {
            Debug.WriteLine("---AddQualityRegisterViewModel Constructing");
            OpenJobsQueryResults = null;
            OpenJobsToDisplay = null;
            refreshLogFlag = false;

            #region State Machine Actions
            AddQualityRegisterSM = new AddQualityRegisterSM
                (
                    beginSelectingJobAction: () => BeginSelectingJob(),
                    queryJobAction: () => GetFilteredOpenJobs(),
                    filterQueryAction: () => FilterJobsToDisplay(),
                    showQueryAction: () => ShowQueryResults(),
                    clearDisplayAction: () => ClearDisplay(),
                    getOperationsAction: () => GetOperations(),
                    getEmployeesAction: () => GetEmployees(),
                    clearSelectedEmployeeAction: () => ClearSelectedEmployee(),
                    clearSelectedOperationAction: () => ClearSelectedOperation(),
                    resetInputAction: () => ResetInput(),
                    setDialogAction: () => SetDialog(),
                    AdornerTextAction: () => SetAdornerText(),
                    cancelQueriesAction: () => CancelQueries(),
                    ExitAction: () => Exit()
                );
            #endregion

            AddQualityRegisterSM.Fire(Triggers.Starting);

            ClearSelectedJobCommand = new RelayCommand(ClearSelectedJob);
            ClearSelectedOperationCommand = new RelayCommand(ClearSelectedOperation);
            ClearSelectedEmployeeCommand = new RelayCommand(ClearSelectedEmployee);
            ExitAddQualityRegisterCommand = new RelayCommand(() => AddQualityRegisterSM.Fire(Triggers.Exit));
            AddQualityRegisterCommand = new RelayCommand(AddQualityRegister, VerifyAddQualityRegister);

            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Log x = new Log();
            x.WriteErrorLog(e);
        }

        #region Accessors
        public JobView SelectedJob
        {
            get
            {
                return _selectedJob;
            }
            set
            {
                if (value != _selectedJob)
                {
                    Set(ref _selectedJob, value);
                    if (_selectedJob != null)
                    {
                        GetSelectedJobImage();
                        AddQualityRegisterSM.Fire(Triggers.SelectJob);
                    }
                }
            }
        }

        //because I cant get the property change to notify when I only change the image in SelectedJob
        public byte[] SelectedJobImage
        {
            get
            {
                return _selectedJobImage;
            }
            set
            {
                Set(ref _selectedJobImage, value);
            }
        }

        public JobOperationDisplay SelectedOperation
        {
            get
            {
                return _selectedOperation;
            }
            set
            {
                if (value != _selectedOperation)
                {
                    Set(ref _selectedOperation, value);
                    if (_selectedOperation != null)
                    {
                        AddQualityRegisterSM.Fire(Triggers.SelectOperation);
                    }

                }


            }
        }
        public EmployeeDisplay SelectedEmployee
        {
            get
            {
                return _selectedEmployee;
            }
            set
            {
                if (value != _selectedEmployee)
                {
                    Set(ref _selectedEmployee, value);
                    if (_selectedEmployee != null)
                    {
                        AddQualityRegisterSM.Fire(Triggers.SelectEmployee);
                    }
                }
            }
        }

        public List<JobView> OpenJobsQueryResults
        {
            get
            {
                return _openJobsQueryResults;
            }
            set
            {
                Set(ref _openJobsQueryResults, value);

            }
        }
        
        public List<JobView> OpenJobsToDisplay
        {
            get
            {
                return _openJobsToDisplay;
            }
            set
            {
                Set(ref _openJobsToDisplay, value);

            }
        }

        public List<JobOperationDisplay> Operations
        {
            get
            {
                return _operations;
            }
            set
            {
                Set(ref _operations, value);
            }
        }
        
        public List<JobOperationDisplay> OperationsToDisplay
        {
            get
            {
                return _operationsToDisplay;
            }
            set
            {
                Set(ref _operationsToDisplay, value);
            }
        }

        private List<EmployeeDisplay> EmployeeQueryResults
        {
            get
            {
                return _employeeQueryResults;
            }
            set
            {
                Set(ref _employeeQueryResults, value);
            }
        }
        
        public List<EmployeeDisplay> EmployeesToDisplay
        {
            get
            {
                return _employeesToDisplay;
            }
            set
            {
                Set(ref _employeesToDisplay, value);
            }
        }

        public string JobToBeSearched
        {
            get
            {
                return _jobToBeSearched;
            }
            set
            {
                Set(ref _jobToBeSearched, value);
                QueryLengthTriggers();
            }
        }

        public string OperationToBeSearched
        {
            get
            {
                return _operationToBeSearched;
            }
            set
            {
                Set(ref _operationToBeSearched, value);
                if (Operations != null) FilterOperationsToDisplay();
            }
        }

        public string EmployeeToBeSearched
        {
            get
            {
                return _employeeToBeSearched;
            }
            set
            {
                Set(ref _employeeToBeSearched, value);
                FilterEmployeesToDisplay();
            }
        }

        public string InputBox
        {
            get
            {
                return _inputBox;
            }
            set
            {
                Set(ref _inputBox, value);
                if (AddQualityRegisterSM.State == States.SelectingOperation)
                {
                    OperationToBeSearched = _inputBox;
                }
                else if (AddQualityRegisterSM.State == States.SelectingEmployee)
                {
                    EmployeeToBeSearched = _inputBox;
                }
                else
                {
                    JobToBeSearched = _inputBox;
                }
            }
        }

        public string UserDialog
        {
            get
            {
                return _userDialog;
            }
            set
            {
                Set(ref _userDialog, value);
            }
        }

        public string AdornerText
        {
            get
            {
                return _adornerText;
            }
            set
            {
                Set(ref _adornerText, value);
            }
        }

        public bool SetupInspectionYes
        {
            get
            {
                return _setupInspectionYes;
            }
            set
            {
                Set(ref _setupInspectionYes, value);
            }
        }

        public bool SetupInspectionNo
        {
            get
            {
                return _setupInspectionNo;
            }
            set
            {
                Set(ref _setupInspectionNo, value);
            }
        }

        #endregion

        private int _lastJobLength = 0;
        //fires triggers based on changes to the job search parameter length
        private void QueryLengthTriggers()
        {

            if (JobToBeSearched.Length < 4)
            {
                AddQualityRegisterSM.Fire(Triggers.JobLengthLess4);

            }
            else if (JobToBeSearched.Length == 4)
            {

                AddQualityRegisterSM.Fire(Triggers.JobLengthIs4);

            }
            else if (JobToBeSearched.Length > _lastJobLength && JobToBeSearched.Length > 4)
            {

                AddQualityRegisterSM.Fire(Triggers.JobLengthGrew);

            }
            else if (JobToBeSearched.Length < _lastJobLength && JobToBeSearched.Length > 4)
            {

                AddQualityRegisterSM.Fire(Triggers.JobLengthShrank);
            }
            _lastJobLength = JobToBeSearched.Length;
        }

        private void FilterJobsToDisplay()
        {
            if (JobToBeSearched.Length > 4)
            {
                var q = (from a in _openJobsQueryResults
                         where a.JobID.StartsWith(JobToBeSearched)
                         select a).ToList();
                OpenJobsToDisplay = q;
                    
                {
                    if (OpenJobsToDisplay.Count == 0)
                    {
                        AddQualityRegisterSM.Fire(Triggers.FilteredResultsEmpty);
                    }
                    else
                    {
                        AddQualityRegisterSM.Fire(Triggers.FilteredResultsNotEmpty);
                    }
                }
            }
            
            Debug.WriteLine("FilterJobsToDisplay() function executed");
        }

        private void FilterOperationsToDisplay()
        {
            if (_operationToBeSearched != "" && _operationToBeSearched != null)
            {
                var q = (from a in _operations
                         where a.OperationID.ToString().StartsWith(OperationToBeSearched)
                         select a).ToList();
                
                OperationsToDisplay = q;
            }
            else
            {
                OperationsToDisplay = Operations;
            }

        }

        private void FilterEmployeesToDisplay()
        {
            if (_employeeToBeSearched != "")
            {
                var q = (from a in _employeeQueryResults
                         //where a.EmployeeName.StartsWith(EmployeeToBeSearched)
                         where a.EmployeeName.StartsWith(EmployeeToBeSearched.ToUpper())
                         select a).ToList();
                
                EmployeesToDisplay = q;
                
            }
            else
            {
                EmployeesToDisplay = EmployeeQueryResults;
            }
        }

        private void SetDialog()
        {
            //Debug.WriteLine("Setting Dialog");
            switch (AddQualityRegisterSM.State)
            {
                case States.SelectingJob:
                    UserDialog = "Type in a Job Number";
                    break;
                case States.sjQueryingJob:
                case States.QueryingOperations:
                    UserDialog = "Searching...";
                    break;
                case States.sjShowingQueryResults:
                case States.sjFilteringQuery:
                case States.sjShowingFilteredResults:
                    UserDialog = "Select a Job";
                    break;
                case States.sjQueryEmpty:
                case States.sjFilteredResultsEmpty:
                    UserDialog = "Job Not Found";
                    break;
                case States.SelectingOperation:
                    UserDialog = "Pick an Operations";
                    break;
                case States.SelectingEmployee:
                    UserDialog = "Who are you?";
                    break;
                default:
                    UserDialog = "";
                    break;
            }
        }

        private void SetAdornerText()
        {
            if (AddQualityRegisterSM.State == States.InsertQR) AdornerText = "Saving...";
        }

        private void ClearDisplay()
        {
            //if (OpenJobsToDisplay != null) OpenJobsToDisplay.Clear();
            //if (OpenJobsQueryResults != null) OpenJobsQueryResults.Clear();
        }  //not used

        private void ClearSelectedJob()
        {
            //SelectedJob = new JobView();
            SelectedJob = null;
            SelectedJobImage = null;
            JobToBeSearched = "";
            ClearSelectedOperation();
            ClearSelectedEmployee();
            Operations = null;
            OperationsToDisplay = null;
            AddQualityRegisterSM.Fire(Triggers.ClearSelectedJob);
        }

        private void ClearSelectedOperation()
        {
            //SelectedOperation = new JobOperationDisplay();
            SelectedOperation = null;
            OperationToBeSearched = "";
            ClearSelectedEmployee();
            AddQualityRegisterSM.Fire(Triggers.ClearSelectedOperation);
        }

        private void ClearSelectedEmployee()
        {
            //SelectedEmployee = new EmployeeDisplay();
            SelectedEmployee = null;
            EmployeeToBeSearched = "";
            AddQualityRegisterSM.Fire(Triggers.ClearSelectedEmployee);
        }

        public override void Cleanup()
        {
            base.Cleanup();
        }

        private void BeginSelectingJob()
        {
            AddQualityRegisterSM.Fire(Triggers.Starting);
        }

        private void ResetInput()
        {
            Debug.WriteLine("ResetInput() Executed");
            if (AddQualityRegisterSM.State == States.SelectingOperation)
            {
                InputBox = "";
            }
            else if (AddQualityRegisterSM.State == States.SelectingEmployee)
            {
                InputBox = EmployeeToBeSearched == null ? "" : EmployeeToBeSearched;
            }
            else if (AddQualityRegisterSM.State == States.SelectingJob)
            {

                InputBox = JobToBeSearched==null? "" : JobToBeSearched;
            }
            else
            {
                InputBox = "";
            }
            SetupInspectionYes = false;
            SetupInspectionNo = false;
        }

        private void Exit()
        {
            //TODO:cancel queries if required


            ShowQualityRegisterLogMessage msg = new ShowQualityRegisterLogMessage();
            msg.RefreshLog = refreshLogFlag;

            Messenger.Default.Send(msg);

            //Clear VM
            ClearSelectedJob();
            OpenJobsQueryResults = null;
            OpenJobsToDisplay = null;
            Operations = null;
            OperationsToDisplay = null;
            ResetInput();
            SetupInspectionYes = false;
            SetupInspectionNo = false;
            AddQualityRegisterSM.Fire(Triggers.Restart);
            
        }

        private void ShowQueryResults()
        {
            if (JobToBeSearched.Length == 4) OpenJobsToDisplay = OpenJobsQueryResults;
            if (JobToBeSearched.Length > 4) FilterJobsToDisplay();
        }

        private bool VerifyAddQualityRegister()
        {
            if (SelectedJob != null
                && SelectedOperation != null
                && SelectedEmployee != null
                && (SetupInspectionYes == true || SetupInspectionNo == true))
                return true;

            return false;
        }

        private void CancelQueries()
        {
            Debug.WriteLine("CancelQueries()");
            if (cts != null)
            {
                Debug.WriteLine("cts NOT NULL");
                cts.Cancel();
            }
           
        }

        #region Data Accessing Functions
        ////<summary>
        ////Will search Open Jobs for JobIDs that start with the string num parameter.
        ////</summary>
        private async void GetFilteredOpenJobs()
        {
            
            Debug.WriteLine("gettingFilteredOpenJobs()");
            try
            {
                cts = new CancellationTokenSource();
                // ***Send a token to carry the message if cancellation is requested.
                var task = await data.GetJobsAsync(JobToBeSearched, cts.Token);
                // await task;
                OpenJobsQueryResults = task;

                if (OpenJobsQueryResults.Count == 0)
                {
                    AddQualityRegisterSM.Fire(Triggers.QueryIsEmpty);
                }
                else
                {
                    AddQualityRegisterSM.Fire(Triggers.QueryNotEmpty);
                    ShowQueryResults();
                }
                
            }
            // *** If cancellation is requested, an OperationCanceledException results.
            catch (OperationCanceledException)
            {
                Debug.WriteLine("Get Jobs Operation Cancelled");
            }
            catch (M1Exception e)
            {
                
            }
        }

        private async void GetSelectedJobImage()
        {
            try
            {
                Debug.WriteLine("Start Getting Job Image");
                cts = new CancellationTokenSource();
                Debug.WriteLine("gettingSelectedJobImage()");
                //waitingForImage = true;
                var task = await data.GetJobImagesAsync(SelectedJob.JobID, SelectedJob.JobAssemblyID, cts.Token);
                //waitingForImage = false;
                Debug.WriteLine("Done Getting Job Image");
                if (SelectedJob != null)
                {
                    SelectedJobImage = task; //because I cant get the property change to notify when I only change the image in SelectedJob
                    SelectedJob.PartImage = SelectedJobImage;
                }
            }
            catch (OperationCanceledException)
            {
                Debug.WriteLine("Get Image Operation Cancelled");
            }
            catch (M1Exception ex)
            {

            }
            
        }

        private async void GetOperations()
        {
            if (Operations == null)
            {
                cts = new CancellationTokenSource();
                try
                {
                    Debug.WriteLine("Start Getting Operations");
                    var task = await data.GetOperations(SelectedJob, cts.Token);
                    Debug.WriteLine("Done Getting Operations");
                    Operations = task;
                    OperationsToDisplay = task;
                    FilterOperationsToDisplay();
                    AddQualityRegisterSM.Fire(Triggers.QueryOperationSuccessful);
                }
                catch (OperationCanceledException)
                {
                    Debug.WriteLine("Get Operations Operation Cancelled");
                }
                catch (M1Exception e)
                {
                    
                }
                
            }
            
        }

        private async void GetEmployees()
        {
            if(_employeeQueryResults == null)
            {
                try
                {
                    var task = await data.GetShopEmployees();
                    EmployeeQueryResults = task;
                    EmployeesToDisplay = EmployeeQueryResults;
                    FilterEmployeesToDisplay();
                }
                catch (OperationCanceledException)
                {
                    Debug.WriteLine("Get Employees Operation Cancelled");
                }
                catch (M1Exception e)
                {
                }
            }
            
        }


        private async void AddQualityRegister()
        {
            AddQualityRegisterSM.Fire(Triggers.InsertingQR);
            

            try
            {
                var task =  data.InsertQualityRegister(SelectedJob, SelectedOperation, SelectedEmployee, SetupInspectionYes);
                await task;
                AdornerText = "Added!";
                await Task.Delay(1000);

                UpdateQualityRegisterLogMessage msg = new UpdateQualityRegisterLogMessage(task.Result);
                Messenger.Default.Send(msg);

                refreshLogFlag = true;

                AddQualityRegisterSM.Fire(Triggers.InsertSuccessful);
                //ExitAddQualityRegister(true);
            }
            catch (M1Exception e)
            {

            }
            catch (OperationCanceledException)
            {
                Debug.WriteLine("Get Employees Operation Cancelled");
            }

            
            
            

        }
        #endregion

        

        
    }
}
