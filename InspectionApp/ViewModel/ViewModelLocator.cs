﻿/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocatorTemplate xmlns:vm="clr-namespace:InspectionApp.ViewModel"
                                   x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"
*/

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using InspectionApp.Model;
using GalaSoft.MvvmLight.Messaging;
using System.Diagnostics;
using EF_M1;

namespace InspectionApp.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// <para>
    /// See http://www.mvvmlight.net
    /// </para>
    /// </summary>
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            if (ViewModelBase.IsInDesignModeStatic)
            {
                SimpleIoc.Default.Register<IDataService, Design.DesignDataService>();
            }
            else
            {
                SimpleIoc.Default.Register<IDataService, DataService>();
            }

            SimpleIoc.Default.Register<AddQualityRegisterViewModel>();
            SimpleIoc.Default.Register<QualityRegisterLogViewModel>();
            SimpleIoc.Default.Register<StartInspectionViewModel>();
        }

        /// <summary>
        /// Gets the Main property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance","CA1822:MarkMembersAsStatic",Justification = "This non-static member is needed for data binding purposes.")]

        public AddQualityRegisterViewModel Add
        {
            get
            {
                return ServiceLocator.Current.GetInstance<AddQualityRegisterViewModel>();
            }
        }

        public QualityRegisterLogViewModel Log
        {
            get
            {
                return ServiceLocator.Current.GetInstance<QualityRegisterLogViewModel>();
            }
        }

        public StartInspectionViewModel Inspection
        {
            get
            {
                return ServiceLocator.Current.GetInstance<StartInspectionViewModel>();
            }
        }

        /// <summary>
        /// Cleans up all the resources.
        /// </summary>
        public static void Cleanup()
        {
            if (SimpleIoc.Default.IsRegistered<AddQualityRegisterViewModel>())
            {
                Debug.Write("SimpleIoC is Registered for AddQaulityRegisterViewModel !!!!!!!");
                SimpleIoc.Default.Unregister<AddQualityRegisterViewModel>();
            }
            Debug.WriteLine("View Model Locator Cleanup() Executed");
        }
    }
}