﻿using GalaSoft.MvvmLight;
using System.Linq;
using Model;
using EF_M1;
using GalaSoft.MvvmLight.CommandWpf;
using System.Windows.Input;
using GalaSoft.MvvmLight.Messaging;
using InspectionApp.Messages;
using System.Diagnostics;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.IO;
using Logger;

namespace InspectionApp.ViewModel
{
    public class QualityRegisterLogViewModel : ViewModelBase
    {
        DataHelper data = new DataHelper();

        private ObservableCollection<QualityRegisterQueue> _qualityRegisterItems;
        private QualityRegisterQueue _selectedQualityRegisterItem;
        private bool _qualityRegisterItemIsSelected;
        private bool _selectedQualityRegisterItemIsInspecting;
        private string _inspectionButtonText;
        private bool _addQualityRegisterViewVisible;
        private bool _startInspectionViewVisible;
        private bool _isSearching;
        private bool _unableToConnect;

        public ICommand AddQualityRegisterCommand { get; set; }
        public ICommand StartInspectionCommand { get; set; }
        public ICommand GetQualityRegisterCommand { get; set; }
        public ICommand LogFileCommand { get; set; }

        public QualityRegisterLogViewModel()
        {
            Debug.WriteLine("--QualityRegisterLogViewModel Constructing");
            #region dataservice handler
            //_dataService = dataService;
            //_dataService.GetData(
            //    (item, error) =>
            //    {
            //        if (error != null)
            //        {
            //            // Report error here
            //            return;
            //        }
            //        //
            //    });
            #endregion

            GetQualityRegisterQueueAsync();

            AddQualityRegisterCommand = new RelayCommand(ShowAddQualityRegisterView, () => !UnableToConnect);
            StartInspectionCommand = new RelayCommand(ShowStartInspectionView);
            GetQualityRegisterCommand = new RelayCommand(GetQualityRegister);

            //Messenger.Default.Register<WindowLoadedMessage>(this, GetQualityRegisterQueueAsync);
            Messenger.Default.Register<ShowQualityRegisterLogMessage>(this, OnShowQualityLogMessage);
            Messenger.Default.Register<UpdateQualityRegisterLogMessage>(this, OnUpdateQualityLogMessage);
            
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Log x = new Log();
            x.WriteErrorLog(e);
        }

        private void LogFile()
        {
            Exception ex = new Exception();
            Log x = new Log();
            UnhandledExceptionEventArgs e = new UnhandledExceptionEventArgs(ex, true);
            
        }

        private async void GetQualityRegister()
        {
            await GetQualityRegisterQueueAsync();
        }
        
        private async void GetQualityRegisterQueueAsync(WindowLoadedMessage msg)
        {
            await GetQualityRegisterQueueAsync();
        }

        private async Task GetQualityRegisterQueueAsync()
        {
            UnableToConnect = false;
            Debug.WriteLine("QualityRegisterLogViewModel.GetQualityRegisterQueueAsync() Executing");
            try
            {
                var task = data.GetQualityRegisterAsync();
                IsSearching = true;
                await task.ConfigureAwait(false);
                IsSearching = false;
                QualityRegisterItems = new ObservableCollection<QualityRegisterQueue>(task.Result);
                OrderQualityRegisterItems();
            }
            catch (M1Exception ex)
            {
                IsSearching = false;
                UnableToConnect = true;
                Debug.WriteLine(ex.Message);
                Debug.WriteLine("QualityRegisterLogViewModel.GetQualityRegisterQueue() Operation Failed");
            }
            
        }


        //This function controls the order in which the QualityRegisterItems are displayed.
        //Should eventually implement a priority algorithm 
        //might be able to store the sort parameters in the DB for remote updating?
        private void OrderQualityRegisterItems()
        {
            QualityRegisterItems = new ObservableCollection<QualityRegisterQueue> 
                    ((from a in QualityRegisterItems
                     orderby a.AssignedDate.HasValue descending, a.AssignedDate, a.OpenedDate
                     select a).ToList());
        }

        #region Accessors
        
        public ObservableCollection<QualityRegisterQueue> QualityRegisterItems
        {
            get
            {
                if (_qualityRegisterItems == null) ;// GetQualityRegisterQueueAsync();
                return _qualityRegisterItems;
            }
            set
            {
                Set(ref _qualityRegisterItems, value);
            }
        }

        public QualityRegisterQueue SelectedQualityRegisterItem
        {
            get
            {
                return _selectedQualityRegisterItem;
            }
            set
            {
                if (value != _selectedQualityRegisterItem)
                {
                    Set(ref _selectedQualityRegisterItem, value);
                    if (_selectedQualityRegisterItem != null)
                    {
                        QualityRegisterItemIsSelected = true;
                        
                    }
                    else
                    {
                        QualityRegisterItemIsSelected = false;
                    }

                    //will set the QualityRegsterItem state for if it is currently inspecting or not
                    if (_selectedQualityRegisterItem != null)
                    {
                        if (_selectedQualityRegisterItem.AssignedDate != null)
                        {
                            SelectedQualityRegisterItemIsInspecting = true;
                        }
                        else
                        {
                            SelectedQualityRegisterItemIsInspecting = false;
                        }
                    }
                    
                    
                }
                
            }
        }
        
        public bool QualityRegisterItemIsSelected
        {
            get
            {
                return _qualityRegisterItemIsSelected;
            }
            set
            {
                Set(ref _qualityRegisterItemIsSelected, value);
            }
        }
        
        public bool SelectedQualityRegisterItemIsInspecting
        {
            get
            {
                return _selectedQualityRegisterItemIsInspecting;
            }
            set
            {
                Set(ref _selectedQualityRegisterItemIsInspecting, value);

                if (_selectedQualityRegisterItemIsInspecting == true)
                {
                    InspectionButtonText = "End or Edit";
                } else {
                    InspectionButtonText = "Begin Inspection";
                }
            }
        }
        
        public string InspectionButtonText
        {
            get
            {
                if (_inspectionButtonText == null) _inspectionButtonText = "Begin Inspection";
                return _inspectionButtonText;
            }
            set
            {
                Set(ref _inspectionButtonText, value);
            }
        }
        
        public bool AddQualityRegisterViewVisible
        {
            get
            {
                return  _addQualityRegisterViewVisible;
            }
            set
            {
                Set(ref _addQualityRegisterViewVisible, value);
            }
        }
        
        public bool StartInspectionViewVisible
        {
            get
            {
                
                return  _startInspectionViewVisible;
            }
            set
            {
                Set(ref _startInspectionViewVisible, value);
            }
        }
        
        public bool IsSearching
        {
            get
            {
                return _isSearching;
            }
            set
            {
                Set(ref _isSearching, value);
            }
        }

        public bool UnableToConnect
        {
            get
            {
                return _unableToConnect;
            }
            set
            {
                Set(ref _unableToConnect, value);
            }
        }

        #endregion


        private void ShowAddQualityRegisterView()
        {
            AddQualityRegisterViewVisible = true;
        }

        private void ShowStartInspectionView()
        {
            StartInspectionViewVisible = true;
            Messenger.Default.Send(new SelectedQualityRegisterMessage
            {
                SelectedQualityRegister = _selectedQualityRegisterItem
            });
        }

        private void OnShowQualityLogMessage(ShowQualityRegisterLogMessage msg)
        {
            SelectedQualityRegisterItem = null;
            StartInspectionViewVisible = false;
            AddQualityRegisterViewVisible = false;
            //if (msg.RefreshLog == true) GetQualityRegisterQueueAsync();
        }


        //Will handle Add, Update, and Remove for items on the list.  This is to avoid having to requery the DB for every single operation.
        //Dangerous, but will save a lot of communication time from the slow-as-fuck wifi.  
        private void OnUpdateQualityLogMessage(UpdateQualityRegisterLogMessage obj)
        {
            if (obj.Removing == true)
            {
                QualityRegisterQueue temp = (from a in QualityRegisterItems where a.QualityRegisterID.Trim() == obj.RemovedQualityRegisterID.Trim() select a).SingleOrDefault();
                QualityRegisterItems.Remove(temp);
            }
            else
            {
                if (QualityRegisterItems.Contains(obj.ChangedQualityRegisterQueue))
                {
                    QualityRegisterItems.Remove(obj.ChangedQualityRegisterQueue);
                    QualityRegisterItems.Add(obj.ChangedQualityRegisterQueue);
                    OrderQualityRegisterItems();
                }
                else
                {
                    //changed item is not present in list, so it will be added
                    QualityRegisterItems.Add(obj.ChangedQualityRegisterQueue);
                }
            }
            
        }


    }

}
