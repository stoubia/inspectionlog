﻿using EF_M1;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using InspectionApp.Messages;
using Logger;
using Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace InspectionApp.ViewModel
{
    public class StartInspectionViewModel : ViewModelBase
    {
        DataHelper data = new DataHelper();

        public StartInspectionSM StartInspectionSM { get; set; }

        public ICommand StartInspectionCommand { get; set; }
        public ICommand StopInspectionCommand { get; set; }
        public ICommand CancelCommand { get; set; }
        public ICommand ChangeInspectorCommand { get; set; }
        public ICommand InspectionPassCommand { get; set; }
        public ICommand InspectionFailCommand { get; set; }
        public ICommand ChangeScrapReasonCommand { get; set; }

        private QualityRegisterQueue _selectedQualityRegister;
        private List<EmployeeDisplay> _inspectorsToDisplay;
        private EmployeeDisplay _selectedInspector;
        private EmployeeDisplay oldInspector;
        private List<Reason> _scrapReasonsToDisplay;
        private Reason _selectedScrapReason;
        private bool? _isPassed;
        //private bool? _isFailed;
        private bool refreshLogFlag;
        private string _adornerText;
        private bool _unableToConnect;


        public StartInspectionViewModel()
        {
            Debug.WriteLine("----StartInspectionViewModel Constructing");
            refreshLogFlag = false;

            #region StateMachine Actions
            StartInspectionSM = new StartInspectionSM
                (
                    ExitAction: () => Exit(),
                    UpdateAction: () => UpdateInspection(),
                    CloseInspectionAction: () => CloseInspection(),
                    FindCurrentStateAction: () => FindCurrentInspectionState(),
                    CheckSelectedInspectorAction: () => CheckSelectedInspector(),
                    StartInspectionAction: () => StartInspectionAsync(),
                    AdornerTextAction: () => SetAdornerText()
                );
            #endregion

            StartInspectionCommand = new RelayCommand(StartInspection, IsStartable);
            StopInspectionCommand = new RelayCommand(StopInspection, IsClosable);
            CancelCommand = new RelayCommand(Cancel);
            ChangeInspectorCommand = new RelayCommand(ChangeInspector);
            InspectionPassCommand = new RelayCommand(InspectionPass);
            InspectionFailCommand = new RelayCommand(InspectionFail);
            ChangeScrapReasonCommand = new RelayCommand(ChangeScrapReason);

            Messenger.Default.Register<SelectedQualityRegisterMessage>(this, GetSelectedQualityRegister);

            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
        }

         
        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Log x = new Log();
            x.WriteErrorLog(e);
        }

        #region Accessors
        public QualityRegisterQueue SelectedQualityRegister
        {
            get
            {
                return _selectedQualityRegister;
            }
            set
            {
                Set(ref _selectedQualityRegister, value);
            }
        }
        public List<EmployeeDisplay> InspectorsToDisplay
        {
            get
            {
                return _inspectorsToDisplay;
            }
            set
            {
                Set(ref _inspectorsToDisplay, value);
            }
        }
        public EmployeeDisplay SelectedInspector
        {
            get
            {
                return _selectedInspector;
            }
            set
            {
                if (value != _selectedInspector)
                {
                    Set(ref _selectedInspector, value);
                    if (_selectedInspector != null)
                    {
                        StartInspectionSM.Fire(InspTriggers.SelectInspector);

                        if (StartInspectionSM.IsInState(InspStates.ChangingInspector))
                        {
                            if (_selectedInspector == oldInspector)
                            {
                                StartInspectionSM.Fire(InspTriggers.InspectorNotChange);
                            }
                            else
                            {
                                StartInspectionSM.Fire(InspTriggers.InspectorChanged);
                            }
                        }
                        Debug.WriteLine("Showing Selected Inspector");


                    }
                    else
                    {
                        Debug.WriteLine("Showing Inspector Selection");
                    }
                }
            }
        }
        public List<Reason> ScrapReasonsToDisplay
        {
            get
            {
                return _scrapReasonsToDisplay;
            }
            set
            {
                Set(ref _scrapReasonsToDisplay, value);
            }
        }
        public Reason SelectedScrapReason
        {
            get
            {
                return _selectedScrapReason;
            }
            set
            {
                if (_selectedScrapReason != value)
                {
                    Set(ref _selectedScrapReason, value);
                    if (_selectedScrapReason != null)
                    {
                        StartInspectionSM.Fire(InspTriggers.SelectScrapReason);
                    }
                }
            }
        }
        public bool? IsPassed
        {
            get
            {
                return _isPassed;
            }
            set
            {
                Set(ref _isPassed, value);
            }
        }
        //public bool? IsFailed
        //{
        //    get
        //    {
        //        return _isFailed;
        //    }
        //    set
        //    {
        //        Set(ref _isFailed, value);
        //    }
        //}

        public string AdornerText
        {
            get
            {
                return _adornerText;
            }
            set
            {
                Set(ref _adornerText, value);
            }
        }

        public bool UnableToConnect
        {
            get
            {
                return _unableToConnect;
            }
            set
            {
                Set(ref _unableToConnect, value);
            }
        }

        #endregion


        //Gets the SelectedQualityRegister object from the message from QualityRegisterLogViewModel
        private async void GetSelectedQualityRegister(SelectedQualityRegisterMessage obj)
        {
            if (_inspectorsToDisplay == null || _scrapReasonsToDisplay == null)  await GetDetailData();
            if (!UnableToConnect)
            {
                SelectedQualityRegister = obj.SelectedQualityRegister;
                if (SelectedQualityRegister.AssignedDate != null)
                {
                    SelectedInspector = (from a in InspectorsToDisplay where a.EmployeeID.Trim() == SelectedQualityRegister.AssignedToEmployeeID.Trim() select a).SingleOrDefault();
                    StartInspectionSM.Fire(InspTriggers.IsInspecting);
                }
                else
                {
                    StartInspectionSM.Fire(InspTriggers.IsInQueue);
                }
            }
            else
            {
                await Task.Delay(3000);
                StartInspectionSM.Fire(InspTriggers.Exit);
                UnableToConnect = false;
            }

        }

        /// <summary>
        /// Gets the Inspectors list and ScrapReasons list from M1, throwing exception if unable to connect to the DB.
        /// </summary>
        private async Task GetDetailData()
        {
            UnableToConnect = false;
            try
            {
                var task1 = data.GetInspectorsAsync();
                
                await task1.ConfigureAwait(false);
                var task2 = data.GetScrapReasonsAsync();
                await task2;

                InspectorsToDisplay = task1.Result;
                ScrapReasonsToDisplay = task2.Result;
                Debug.WriteLine("Done Getting Detail Data");
            }
            catch (M1Exception ex)
            {
                UnableToConnect = true;
               // Debug.WriteLine(ex.Message);
            }

        }

        
        
        private async void StartInspectionAsync()
        {
            try
            {
                var task = data.CreateInspection(SelectedInspector, SelectedQualityRegister);
                await task;
                Debug.WriteLine("Done waiting for InsertQualityRegister()");
                refreshLogFlag = true;

                //Send Message to the QualityRegisterLogViewModel to update the item that has had its state changed to Inspecting
                UpdateQualityRegisterLogMessage msg = new UpdateQualityRegisterLogMessage(SelectedQualityRegister);
                msg.ChangedQualityRegisterQueue.AssignedDate = DateTime.Now;  //HACKYSACKY i should get this returned from the DataHelper but i dgaf
                msg.ChangedQualityRegisterQueue.AssignedToEmployeeName = SelectedInspector.EmployeeName;
                msg.ChangedQualityRegisterQueue.AssignedToEmployeeID = SelectedInspector.EmployeeID;
                Messenger.Default.Send(msg);

                StartInspectionSM.Fire(InspTriggers.InsertSuccessful);
            }
            // *** If cancellation is requested, an OperationCanceledException results.
            //catch (OperationCanceledException)
            //{
            //    Debug.WriteLine("Get Jobs Operation Cancelled");
            //}
            catch (M1Exception e)
            {
                
            }
            
            
        }

        //as of now all you can update is the InspectorID
        private async void UpdateInspection()
        {
            try
            {
                var task = data.UpdateQualityRegister(SelectedInspector, SelectedQualityRegister);
                await task;
                SelectedQualityRegister = task.Result;
                refreshLogFlag = true;

                UpdateQualityRegisterLogMessage msg = new UpdateQualityRegisterLogMessage(SelectedQualityRegister);
                msg.ChangedQualityRegisterQueue.AssignedToEmployeeName = SelectedInspector.EmployeeName;
                Messenger.Default.Send(msg);

                StartInspectionSM.Fire(InspTriggers.UpdateSuccessful); 
            }
            catch (M1Exception ex)
            {

            }
        }

        private async void CloseInspection()
        {
            try
            {
                var task = data.EndInspectionAsync(SelectedQualityRegister, SelectedScrapReason);
                await task;
                UpdateQualityRegisterLogMessage msg = new UpdateQualityRegisterLogMessage(task.Result);
                Messenger.Default.Send(msg);
                refreshLogFlag = true;
                Messenger.Default.Send(new ShowQualityRegisterLogMessage());
                StartInspectionSM.Fire(InspTriggers.CloseSuccessful);
            }
            // *** If cancellation is requested, an OperationCanceledException results.
            //catch (OperationCanceledException)
            //{
            //    Debug.WriteLine("Get Jobs Operation Cancelled");
            //}
            catch (M1Exception ex)
            { 
            }

            
        }

        private void FindCurrentInspectionState()
        {
            if (IsPassed == true)
            {
                StartInspectionSM.Fire(InspTriggers.IsPass);
            }
            else if (!IsPassed == true && SelectedScrapReason == null)
            {
                StartInspectionSM.Fire(InspTriggers.IsFail);
            }
            else if (!IsPassed == true && SelectedScrapReason != null)
            {
                StartInspectionSM.Fire(InspTriggers.IsFailWithReason);
            }
        }

        private void CheckSelectedInspector()
        {
            if (SelectedInspector == null) SelectedInspector = oldInspector;
        }

        private void SetAdornerText()
        {
            if (StartInspectionSM.State == InspStates.Inserting) AdornerText = "Starting Inspection...";
            if (StartInspectionSM.State == InspStates.UpdatingInspector) AdornerText = "Updating...";
            if (StartInspectionSM.State == InspStates.Closing) AdornerText = "Closing...";
        }

        //Start Inspection Command Validation
        //Requires an inspector to be selected
        private bool IsStartable()
        {
            if (StartInspectionSM.IsInState(InspStates.Startable))
            {
                if (SelectedInspector == null) return false;
                return true;
            }
            return false;
        }

        private bool IsClosable()
        {
            if (SelectedInspector == null) return false;
            if (StartInspectionSM.IsInState(InspStates.ClosablePassed))
            {
                return true;
            }
            else if (StartInspectionSM.IsInState(InspStates.ClosableFailed))
            {
                if (SelectedScrapReason != null) return true;
            }
            return false;
        }

        private void Exit()
        {
            Messenger.Default.Send(new ShowQualityRegisterLogMessage { RefreshLog = refreshLogFlag });
            refreshLogFlag = false;
            StartInspectionSM.Fire(InspTriggers.Restart);
            //Clear VM
            SelectedInspector = null;
            SelectedQualityRegister = null;
            SelectedScrapReason = null;
            IsPassed = null;
            //IsFailed = null;

        }


        #region Command Functions
        private void StartInspection()
        {
            StartInspectionSM.Fire(InspTriggers.StartInspection);
        }

        private void StopInspection()
        {
            StartInspectionSM.Fire(InspTriggers.CloseInspection);
        }

        private void Cancel()
        {
            StartInspectionSM.Fire(InspTriggers.Exit);
        }

        private void ChangeInspector()
        {
            oldInspector = SelectedInspector; 
            SelectedInspector = null;
            StartInspectionSM.Fire(InspTriggers.ChangeInspector);
            
        }

        private void InspectionPass()
        {
            if (IsPassed==false || IsPassed == null)
            {
                IsPassed = true;
                SelectedScrapReason = null;
                StartInspectionSM.Fire(InspTriggers.Pass);
            }
            
        }

        private void InspectionFail()
        {
            if (IsPassed == true || IsPassed == null)
            {
                IsPassed = false;
                StartInspectionSM.Fire(InspTriggers.Fail);

            } 
        }

        private void ChangeScrapReason()
        {
            SelectedScrapReason = null;
            StartInspectionSM.Fire(InspTriggers.ChangeScrapReason);
        }
        #endregion


    }
}
