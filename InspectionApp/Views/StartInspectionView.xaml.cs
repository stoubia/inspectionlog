﻿using InspectionApp.ViewModel;
using Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InspectionApp.Views
{
    /// <summary>
    /// Interaction logic for StartInspectionView.xaml
    /// </summary>
    public partial class StartInspectionView : UserControl
    {
        public StartInspectionView()
        {
           
            InitializeComponent();
        }

        private void tglPass_Click(object sender, RoutedEventArgs e)
        {
            //if (tglPass.IsChecked == true) tglFail.IsChecked = false;
        }
        private void tglFail_Click(object sender, RoutedEventArgs e)
        {
            //if (tglFail.IsChecked == true) tglPass.IsChecked = false;
        }

       

        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            tglPass.IsChecked = false;
            tglFail.IsChecked = false;
        }

        

       

      


       
    }
}
