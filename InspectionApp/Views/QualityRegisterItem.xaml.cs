﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InspectionApp.Views
{
    /// <summary>
    /// Interaction logic for QualityRegisterItem.xaml
    /// </summary>
    public partial class QualityRegisterItem : UserControl
    {
        public QualityRegisterItem()
        {
            InitializeComponent();
        }


        private void tbkAssy_Loaded(object sender, RoutedEventArgs e)
        {
            //Debug.WriteLine(String.Format("Loaded, tbkAssy.Text = {0}", tbkAssy.Text));
            if (tbkAssy.Text == "A0")
            {
                tbkAssy.Visibility = Visibility.Collapsed;
                                //rowDefinition.Height = new GridLength(0.5, GridUnitType.Star);
                //grdAssyRow;
               
                //Debug.WriteLine(String.Format("Collapsing, tbkAssy.Text = {0}", tbkAssy.Text));
            }
            else
            {
               // Debug.WriteLine(String.Format("Not Collapsing, tbkAssy.Text = {0}", tbkAssy.Text));
            }
        }

        private void tbkAssignedDate_Loaded(object sender, RoutedEventArgs e)
        {
            if (tbkAssignedDate.Text == "")
            {
               // Debug.WriteLine("Assigned Date is Empty");
                tbkInspectionStarted.Visibility = Visibility.Collapsed;
                tbkAssignedDate.Visibility = Visibility.Collapsed;
                tbkAssignedHour.Visibility = Visibility.Collapsed;
                tbkAssignedEmployee.Visibility = Visibility.Collapsed;
            }
            else
            {
                //Debug.WriteLine(String.Format("Assigned Date is = {0}", tbkAssignedDate.Text));
            }
        }

        


    }
}
