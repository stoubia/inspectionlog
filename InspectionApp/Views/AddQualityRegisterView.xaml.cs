﻿using GalaSoft.MvvmLight.Messaging;
using InspectionApp.Messages;
using InspectionApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace InspectionApp.Views
{
    /// <summary>
    /// Interaction logic for AddQualityRegisterView.xaml
    /// </summary>
    public partial class AddQualityRegisterView : UserControl
    {

       
        //DispatcherTimer timer = new DispatcherTimer();
        
        public AddQualityRegisterView()
        {
            

            InitializeComponent();
            //Debug.WriteLine("AddQualityRegisterView Initialized");
            
            //            timer.Interval = TimeSpan.FromSeconds(5);
            //            timer.Tick += timer_Tick;
            //            timer.Start();

                
        }

        //void timer_Tick(object sender, EventArgs e)
        //{
        //    Debug.WriteLine("Timer Ticked!!!!!!!!!!!!!!!");
        //    //var vm = (AddQualityRegisterViewModel)DataContext;
        //    timer.Stop();
        //    Debug.WriteLine("Timer Stopped");
        //    Messenger.Default.Send(new ExitAddQualityRegisterViewMessage());
        //    //vm.ExitCommand.Execute(null);
            
        //}



        public void KeyboardKeystroke(string input)
        {
            switch (input)
            {
                case "back":
                    if (txtInputBox.Text.Length > 0) txtInputBox.Text = txtInputBox.Text.Remove(txtInputBox.Text.Length - 1);
                    break;
                default:
                    txtInputBox.Text += input;
                    break;
            }
        }

        public void Keystroke(string input)
        {
            switch (input)
            {
                case "del":
                    if (txtInputBox.Text.Length > 0) txtInputBox.Text = txtInputBox.Text.Remove(txtInputBox.Text.Length - 1);
                    break;
                default:
                    txtInputBox.Text += input;
                    break;
            }
        }

        public void btnKeyboard_Click(object sender, RoutedEventArgs e)
        {
            Button _button = (Button)sender;
            KeyboardKeystroke(_button.Tag.ToString());
            Debug.WriteLine("Keyboard input: {0}", _button.Tag.ToString());
        }

        #region Numberpad Keys
        public void btn1_Click(object sender, RoutedEventArgs e)
        {
            Keystroke("1");
        }

        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            Keystroke("2");
        }

        private void btn3_Click(object sender, RoutedEventArgs e)
        {
            Keystroke("3");
        }

        private void btn4_Click(object sender, RoutedEventArgs e)
        {
            Keystroke("4");
        }

        private void btn5_Click(object sender, RoutedEventArgs e)
        {
            Keystroke("5");
        }

        private void btn6_Click(object sender, RoutedEventArgs e)
        {
            Keystroke("6");
        }

        private void btn7_Click(object sender, RoutedEventArgs e)
        {
            Keystroke("7");
        }

        private void btn8_Click(object sender, RoutedEventArgs e)
        {
            Keystroke("8");
        }

        private void btn9_Click(object sender, RoutedEventArgs e)
        {
            Keystroke("9");
        }

        private void btn0_Click(object sender, RoutedEventArgs e)
        {
            Keystroke("0");
        }

        private void btnDel_Click(object sender, RoutedEventArgs e)
        {
            Keystroke("del");
        }

        private void btnDash_Click(object sender, RoutedEventArgs e)
        {
            Keystroke("-");
        }
        #endregion

        //private void svSelectionScroller_SourceUpdated(object sender, DataTransferEventArgs e)
        //{
        //    svSelectionScroller.ScrollToTop();
        //    Debug.WriteLine("SelecitonScroller SourceUpdate");
        //}

        //private void lbJobSelectionList_SourceUpdated(object sender, DataTransferEventArgs e)
        //{
        //    svSelectionScroller.ScrollToTop();
        //    Debug.WriteLine("job Selection List SourceUpdate");
        //}

        private void txtClearInput_Click(object sender, RoutedEventArgs e)
        {
            txtInputBox.Text = "";
        }


        private void tbYes_Click(object sender, RoutedEventArgs e)
        {
            if (tbYes.IsChecked == true) tbNo.IsChecked = false;
        }

        private void tbNo_Click(object sender, RoutedEventArgs e)
        {
            if (tbNo.IsChecked == true) tbYes.IsChecked = false;
        }

    }
}
