﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace InspectionApp
{
    public class AddQualityRegisterSMVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string state = value != null ? value.ToString() : String.Empty;
            string targetState = parameter.ToString();
            Boolean vis;
            bool debugging = false;

            switch(state)
            {
                    
                case "SelectingJob":
                    switch (targetState)
                    {
                        case "Dialog": 
                        case "NumberPad":
                            vis = true; 
                            break;
                        default: vis = false; break;
                    }
                    if(debugging) Debug.WriteLine("Selecting Job Case");
                    break;
                case "sjQueryingJob":
                    switch (targetState)
                    {

                        case "Dialog":
                        case "NumberPad":
                        case "AnimatedClock":
                            vis = true;
                            break;
                        default: vis = false; break;
                    }
                    if (debugging) Debug.WriteLine("Querying Job Case");
                    break;
                case "sjQueryEmpty":
                    switch (targetState)
                    {
                        case "Dialog":
                        case "NumberPad":
                            vis = true;
                            break;
                        default: vis = false; break;
                    }
                    if (debugging) Debug.WriteLine("Query Empty Case");
                    break;
                case "sjShowingQueryResults":
                    switch (targetState)
                    {
                        case "Dialog":
                        case "JobSelection":
                        case "NumberPad":
                            vis = true;
                            break;
                        default: vis = false; break;
                    }
                    if (debugging) Debug.WriteLine("Showing Query Case");
                    break;
                case "sjFilteringQuery":
                    switch (targetState)
                    {
                        case "Dialog":
                        case "JobSelection":
                        case "NumberPad":
                            vis = true;
                            break;
                        default: vis = false; break;
                    }
                    if (debugging) Debug.WriteLine("Filtering Query Case");
                    break;
                case "sjFilteredResultsEmpty":
                    switch (targetState)
                    {
                        case "Dialog":
                        case "NumberPad":
                            vis = true;
                            break;
                        default: vis = false; break;
                    }
                    if (debugging) Debug.WriteLine("Filter Empty Case");
                    break;
                case "sjShowingFilteredResults":
                    switch (targetState)
                    {
                        case "Dialog":
                        case "JobSelection":
                        case "NumberPad":
                            vis = true;
                            break;
                        default: vis = false; break;
                    }
                    if (debugging) Debug.WriteLine("Showing Filtered Results Case");
                    break;
                case "QueryingOperations":
                    switch (targetState)
                    {
                        case "SelectedJob":
                        case "Dialog":
                        case "NumberPad":
                        case "AnimatedClock":
                            vis = true;
                            break;
                        default: vis = false; break;
                    }
                    break;
                case "SelectingOperation":
                    switch (targetState)
                    {
                        case "SelectedJob":
                        case "Dialog":
                        case "OperationSelection":
                        case "NumberPad":
                            vis = true;
                            break;
                        default: vis = false; break;
                    }
                    if (debugging) Debug.WriteLine("Selecting Operation Case");
                    break;
                case "SelectingEmployee":
                    switch (targetState)
                    {
                        case "SelectedJob":
                        case "SelectedOperation":
                        case "Dialog":
                        case "EmployeeSelection":
                        case "Keyboard":
                            vis = true;
                            break;
                        default: vis = false; break;
                    }
                    if (debugging) Debug.WriteLine("Selecting Employee Case");
                    break;
                case "SelectingOtherOptions":
                    switch (targetState)
                    {
                        case "SelectedJob":
                        case "SelectedOperation":
                        case "SelectedEmployee":
                        case "OtherOptions":
                        case "Dialog":
                            vis = true;
                            break;
                        default: vis = false; break;
                    }
                    if (debugging) Debug.WriteLine("Selecting Other Case");
                    break;
                case "InsertQR":
                switch (targetState)
                {
                    case "SelectedJob":
                    case "SelectedOperation":
                    case "SelectedEmployee":
                    case "OtherOptions":
                    case "Dialog":
                    case "Adorner":
                        vis = true;
                        break;
                    default: vis = false; break;
                }
                if (debugging) Debug.WriteLine("Selecting Other Case");
                break;
                    
                default:
                    vis = true;
                    if (debugging) Debug.WriteLine("Other Case");
                    break;
            }
             return vis == true ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
