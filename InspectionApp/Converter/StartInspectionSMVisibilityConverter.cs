﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace InspectionApp.Converter
{
    public class StartInspectionSMVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string state = value != null ? value.ToString() : String.Empty;
            string targetState = parameter.ToString();
            Boolean vis;
            bool debugging = false;

            switch(state)
            {
                case "Start":
                    switch (targetState)
                    {
                        default: vis = false; break;
                    }
                    break;
                case "InQueue":
                    switch (targetState)
                    {
                        case "InspectorSelector":
                        case "StartInspectionCommand":
                            vis = true;
                            break;
                        default: vis = false; break;
                    }
                    break;
                case "Startable":
                    switch (targetState)
                    {
                        case "SelectedInspector":
                        case "StartInspectionCommand":
                            vis = true;
                            break;
                        default: vis = false; break;
                    }
                    break;
                case "Inserting":
                    switch (targetState)
                    {
                        case "SelectedInspector":
                        case "StartInspectionCommand":
                        case "Adorner":
                            vis = true;
                            break;
                        default: vis = false; break;
                    }
                    break;
                case "Inspecting":
                    switch (targetState)
                    {
                        case "SelectedInspector":
                        case "EndInspection"    :
                        case "StopInspectionCommand":
                            vis = true;
                            break;
                        default: vis = false; break;
                    }
                    break;
                case "ChangingInspector":
                    switch (targetState)
                    {
                        case "InspectorSelector":
                        case "StopInspectionCommand":
                        case "EndInspection":
                            vis = true;
                            break;
                        default: vis = false; break;
                    }
                    break;
                case "UpdatingInspector":
                    switch (targetState)
                    {
                        case "SelectedInspector":
                        case "EndInspection":
                        case "StopInspectionCommand":
                        case "Adorner":
                            vis = true;
                            break;
                        default: vis = false; break;
                    }
                    break;
                case "SelectingScrapReason":
                    switch (targetState)
                    {
                        case "SelectedInspector":
                        case "EndInspection"    :
                        case "ScrapReasons"     :
                        case "StopInspectionCommand":
                            vis = true;
                            break;
                        default: vis = false; break;
                    }
                    break;
                case "ClosablePassed":
                    switch (targetState)
                    {
                        case "SelectedInspector":
                        case "EndInspection":
                        case "StopInspectionCommand":
                            vis = true;
                            break;
                        default: vis = false; break;
                    }
                    break;
                case "ClosableFailed":
                    switch (targetState)
                    {
                        case "SelectedInspector":
                        case "EndInspection":
                        case "StopInspectionCommand":
                        case "SelectedScrapReason":
                            vis = true;
                            break;
                        default: vis = false; break;
                    }
                    break;
                case "Closing":
                    switch (targetState)
                    {
                        case "SelectedInspector":
                        case "EndInspection":
                        case "StopInspectionCommand":
                        case "Adorner":
                            vis = true;
                            break;
                        default: vis = false; break;
                    }
                    break;
                default:
                    vis = true;
                    if (debugging) Debug.WriteLine("Other Case");
                    break;
            }
             return vis == true ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
