﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_M1
{
    public class ExecutionStrategy:DbExecutionStrategy
    {
        public ExecutionStrategy() 
        { 
        }

        public ExecutionStrategy(int maxRetryCount, TimeSpan maxDelay) : base(maxRetryCount, maxDelay) 
        { 
        }

        protected override bool ShouldRetryOn(Exception ex)
        {
            Debug.WriteLine("ExecutionStrategy.ShouldRetryOn()");
            bool retry = false;
            int retrys = 0;
            SqlException sqlException = ex as SqlException;
            if (sqlException != null)
            {
                Debug.WriteLine("ShouldRetryOn() sqlException !null");
                int[] errorsToRetry = {
                                          1205, //Deadlock
                                          -2,   //Timeout
                                      };
                if (sqlException.Errors.Cast<SqlError>().Any(x => errorsToRetry.Contains(x.Number)))
                {
                    retrys++;
                    Debug.WriteLine("RRRRRRRRRRRRRRRRRRRRRetrys = " + retrys);
                    retry = true;
                    
                }
                else
                {
                    Debug.Write("Execution Strategy error not being handled --- ");
                    Debug.WriteLine(sqlException.ToString());
                }
            }
            if (ex is TimeoutException)
            {
                Debug.WriteLine("ShouldRetryOn() ex is TimeoutException");
                retry = true;
            }
            retrys = 0;
            return retry;
        }
    }

    class M1ConnectionConfiguration : DbConfiguration
    {
        public M1ConnectionConfiguration()
        {
            SetExecutionStrategy("System.Data.SqlClient", () => new ExecutionStrategy());
        }
    }
}
