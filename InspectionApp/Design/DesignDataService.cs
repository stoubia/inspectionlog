﻿using System;
using InspectionApp.Model;
using System.Collections.Generic;
using Model;

namespace InspectionApp.Design
{
    public class DesignDataService : IDataService
    {
        public void GetData(Action<DataItem, Exception> callback)
        {
            // Use this to create design time data
            //design time Quality Queue data
            List<QualityRegisterQueue> queue = new List<QualityRegisterQueue>();

            QualityRegisterQueue temp = new QualityRegisterQueue();
            for(int i = 1; i <= 20; i++)
            {
                temp.JobID = "1234" + i;
                temp.JobAssemblyID = i;
                temp.JobOperationID = i*10;
                temp.OpenedByEmployeeID = "Stefan Toubia";
                temp.OpenedDate = new DateTime(2015, 10, 1, 7, 0+i*2, 0);
                temp.PartID = i + "10034" + i + "-0" + i;
                queue.Add(temp);
            }
            

            //callback(queue, null);
        }
    }
}