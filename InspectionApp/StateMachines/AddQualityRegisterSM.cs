﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace InspectionApp
{

    public enum States
    {
        Start,
        SelectingJob, sjQueryingJob, sjShowingQueryResults, sjQueryEmpty, sjFilteringQuery, sjFilteredResultsEmpty, sjShowingFilteredResults,
        QueryingOperations, SelectingOperation,
        SelectingEmployee,
        SelectingOtherOptions,
        InsertQR,
        End
    }

    public enum Triggers
    {
        Starting,
        SelectJob, SelectEmployee, SelectOperation, ClearSelectedOperation, ClearSelectedEmployee, QRSubmit, Exit, ClearSelectedJob,
        JobLengthIs4, QueryIsEmpty, QueryNotEmpty, JobLengthLess4, JobLengthGrew, FilteredResultsEmpty, FilteredResultsNotEmpty, JobLengthShrank,
        QueryOperationSuccessful, QueryOperationFailed,
        InsertingQR, InsertSuccessful, Restart
    }

    public class AddQualityRegisterSM : Stateless.StateMachine<States, Triggers>, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public AddQualityRegisterSM(Action beginSelectingJobAction, Action queryJobAction, Action filterQueryAction, Action showQueryAction, Action clearDisplayAction,
            Action getOperationsAction, Action getEmployeesAction, Action clearSelectedOperationAction, Action clearSelectedEmployeeAction, Action resetInputAction, 
            Action setDialogAction, Action ExitAction, Action cancelQueriesAction, Action AdornerTextAction)
            : base(States.Start)
        {
            Configure(States.Start)
                .OnEntry(beginSelectingJobAction)
                .Permit(Triggers.Starting, States.SelectingJob);

            Configure(States.SelectingJob)
                .OnEntry(resetInputAction)
                .OnEntry(clearDisplayAction)
                .OnEntryFrom(Triggers.JobLengthLess4, cancelQueriesAction)
                .OnEntryFrom(Triggers.JobLengthShrank, cancelQueriesAction)
                .OnEntryFrom(Triggers.ClearSelectedJob, cancelQueriesAction)
                .Permit(Triggers.JobLengthIs4, States.sjQueryingJob)
                .Permit(Triggers.Exit, States.End)
                .Ignore(Triggers.JobLengthGrew)
                .Ignore(Triggers.JobLengthLess4)
                .Ignore(Triggers.JobLengthShrank)
                .Ignore(Triggers.SelectJob)
                .Ignore(Triggers.SelectOperation);
                
            Configure(States.sjQueryingJob)
                .OnEntry(queryJobAction)
                .Permit(Triggers.JobLengthLess4, States.SelectingJob)
                .Permit(Triggers.JobLengthShrank, States.SelectingJob)
                .Permit(Triggers.QueryNotEmpty, States.sjShowingQueryResults)
                .Permit(Triggers.QueryIsEmpty, States.sjQueryEmpty)
                .Permit(Triggers.Exit, States.End)
                .Ignore(Triggers.JobLengthGrew)
                .Ignore(Triggers.JobLengthIs4)
                ;

            Configure(States.sjShowingQueryResults)
                .OnEntry(showQueryAction)
                .Permit(Triggers.JobLengthLess4, States.SelectingJob)
                .Permit(Triggers.JobLengthGrew, States.sjFilteringQuery)
                .Permit(Triggers.SelectJob, States.QueryingOperations)
                .Permit(Triggers.FilteredResultsEmpty, States.sjFilteredResultsEmpty)
                .Permit(Triggers.FilteredResultsNotEmpty, States.sjShowingFilteredResults)
                .Permit(Triggers.Exit, States.End)
                .Ignore(Triggers.SelectOperation)
                .Ignore(Triggers.JobLengthShrank)
                .Ignore(Triggers.JobLengthIs4);

            Configure(States.sjQueryEmpty)
                .Permit(Triggers.JobLengthLess4, States.SelectingJob)
                .Permit(Triggers.Exit, States.End)
                .Ignore(Triggers.JobLengthIs4)
                .Ignore(Triggers.JobLengthGrew)
                .Ignore(Triggers.JobLengthShrank)
                .Ignore(Triggers.SelectJob);

            Configure(States.sjFilteringQuery)
                .OnEntry(filterQueryAction)
                .Permit(Triggers.FilteredResultsEmpty, States.sjFilteredResultsEmpty)
                .Permit(Triggers.FilteredResultsNotEmpty, States.sjShowingFilteredResults)
                .Permit(Triggers.Exit, States.End);
                

            Configure(States.sjFilteredResultsEmpty)
                .Permit(Triggers.JobLengthLess4, States.SelectingJob)
                .Permit(Triggers.JobLengthIs4, States.sjShowingQueryResults)
                .Permit(Triggers.JobLengthShrank, States.sjFilteringQuery)
                .Permit(Triggers.Exit, States.End)
                .Ignore(Triggers.JobLengthGrew)
                .Ignore(Triggers.SelectOperation)
                .Ignore(Triggers.SelectJob)
                .Ignore(Triggers.FilteredResultsEmpty);

            Configure(States.sjShowingFilteredResults)
                .Permit(Triggers.JobLengthLess4, States.SelectingJob)
                .Permit(Triggers.JobLengthIs4, States.sjShowingQueryResults)
                .Permit(Triggers.JobLengthGrew, States.sjFilteringQuery)
                .Permit(Triggers.JobLengthShrank, States.sjFilteringQuery)
                .Permit(Triggers.SelectJob, States.QueryingOperations)
                .Permit(Triggers.Exit, States.End)
                .Ignore(Triggers.FilteredResultsNotEmpty)
                .Ignore(Triggers.SelectOperation);

            Configure(States.QueryingOperations)
                .OnEntry(getOperationsAction)
                .OnEntry(resetInputAction)
                .Permit(Triggers.QueryOperationSuccessful, States.SelectingOperation)
                .Permit(Triggers.ClearSelectedJob, States.SelectingJob)
                .Permit(Triggers.Exit, States.End)
                .Ignore(Triggers.SelectJob)
                .Ignore(Triggers.SelectEmployee)
                .Ignore(Triggers.ClearSelectedOperation)
                .Ignore(Triggers.ClearSelectedEmployee)
                .Ignore(Triggers.JobLengthGrew)
                .Ignore(Triggers.JobLengthLess4)
                .Ignore(Triggers.JobLengthShrank)
                .Ignore(Triggers.SelectJob)
                .Ignore(Triggers.SelectOperation)
                .Ignore(Triggers.JobLengthIs4);

                
            Configure(States.SelectingOperation)
                .OnEntry(cancelQueriesAction)
                .OnEntry(resetInputAction)
                //.OnEntry(clearSelectedOperationAction)
                .Permit(Triggers.ClearSelectedJob, States.SelectingJob)
                .Permit(Triggers.SelectOperation, States.SelectingEmployee)
                .Permit(Triggers.Exit, States.End)
                .Ignore(Triggers.JobLengthLess4)
                .Ignore(Triggers.SelectJob)
                .Ignore(Triggers.SelectEmployee)
                .Ignore(Triggers.ClearSelectedOperation)
                .Ignore(Triggers.ClearSelectedEmployee);

            Configure(States.SelectingEmployee)
                .OnEntry(resetInputAction)
                .OnEntry(getEmployeesAction)
                //.OnEntry(clearSelectedEmployeeAction)
                .Permit(Triggers.SelectEmployee, States.SelectingOtherOptions)
                .Permit(Triggers.ClearSelectedJob, States.SelectingJob)
                .Permit(Triggers.ClearSelectedOperation, States.SelectingOperation)
                .Permit(Triggers.Exit, States.End)
                .Ignore(Triggers.JobLengthLess4)
                .Ignore(Triggers.SelectJob)
                .Ignore(Triggers.ClearSelectedEmployee)
                .Ignore(Triggers.SelectOperation);

            Configure(States.SelectingOtherOptions)
                .OnEntry(resetInputAction)
                .Permit(Triggers.ClearSelectedJob, States.SelectingJob)
                .Permit(Triggers.ClearSelectedOperation, States.SelectingOperation)
                .Permit(Triggers.ClearSelectedEmployee, States.SelectingEmployee)
                .Permit(Triggers.InsertingQR, States.InsertQR)
                .Permit(Triggers.Exit, States.End)
                .Ignore(Triggers.SelectJob)
                .Ignore(Triggers.SelectOperation)
                .Ignore(Triggers.SelectEmployee)
                .Ignore(Triggers.JobLengthLess4);

            Configure(States.InsertQR)
                .OnEntry(AdornerTextAction)
                .Permit(Triggers.InsertSuccessful, States.End);

            Configure(States.End)
                .OnEntry(cancelQueriesAction)
                .OnEntry(ExitAction)
                .Permit(Triggers.Restart, States.Start)
                .Ignore(Triggers.SelectJob)
                .Ignore(Triggers.SelectOperation)
                .Ignore(Triggers.SelectEmployee)
                .Ignore(Triggers.ClearSelectedJob)
                .Ignore(Triggers.ClearSelectedOperation)
                .Ignore(Triggers.JobLengthLess4)
                .Ignore(Triggers.ClearSelectedEmployee)
                .Ignore(Triggers.JobLengthGrew)
                .Ignore(Triggers.JobLengthShrank)
                ;

            OnTransitioned((t) =>
            {
                setDialogAction();
            });

          OnTransitioned((t) => 
                              {
                                OnPropertyChanged("State");
                                CommandManager.InvalidateRequerySuggested();
                              } );
      
          //used to debug commands and UI components
          OnTransitioned( (t) => Debug.WriteLine("State Machine transitioned from {0} -> {1} [{2}]", t.Source, t.Destination, t.Trigger ));
        }

        private void OnPropertyChanged(string propertyName)
        {
          if(PropertyChanged != null)
          {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
          }
        }

    }
}
