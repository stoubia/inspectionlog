﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace InspectionApp
{

    public enum InspStates
    {
        Start, InQueue, Startable, Inserting,
        Inspecting, ChangingInspector, UpdatingInspector, 
        SelectingScrapReason, ClosablePassed, ClosableFailed, Closing,
        End
        
    }

    public enum InspTriggers
    {
        IsInQueue, SelectInspector, StartInspection, InsertSuccessful, IsInspecting,
        ChangeInspector, Fail, Pass, InspectorNotChange, InspectorChanged, IsPass, IsFail, IsFailWithReason,
        CloseInspection, SelectScrapReason, UpdateSuccessful, CloseSuccessful, ChangeScrapReason,
        Exit, Restart
    }

    public class StartInspectionSM : Stateless.StateMachine<InspStates, InspTriggers>, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public StartInspectionSM(Action ExitAction, Action UpdateAction, Action CloseInspectionAction, Action FindCurrentStateAction,
            Action CheckSelectedInspectorAction, Action StartInspectionAction, Action AdornerTextAction)
            : base(InspStates.Start)
        {
            Configure(InspStates.Start)
                .Permit(InspTriggers.IsInQueue, InspStates.InQueue)
                .Permit(InspTriggers.IsInspecting, InspStates.Inspecting)
                .Permit(InspTriggers.Exit, InspStates.End)
                .Ignore(InspTriggers.SelectInspector);

            Configure(InspStates.InQueue)
                //.OnEntry(GetInspectorsAction)
                .Permit(InspTriggers.SelectInspector, InspStates.Startable)
                .Permit(InspTriggers.Exit, InspStates.End);

            Configure(InspStates.Startable)
                .Permit(InspTriggers.StartInspection, InspStates.Inserting)
                .Permit(InspTriggers.Exit, InspStates.End)
                .Permit(InspTriggers.ChangeInspector, InspStates.InQueue)
                .Ignore(InspTriggers.InspectorNotChange)
                .Ignore(InspTriggers.InspectorChanged)
                .Ignore(InspTriggers.SelectInspector);

            Configure(InspStates.Inserting)
                .OnEntry(AdornerTextAction)
                .OnEntry(StartInspectionAction)
                .Permit(InspTriggers.InsertSuccessful, InspStates.End);


            Configure(InspStates.Inspecting)
                .OnEntry(FindCurrentStateAction)
                .Permit(InspTriggers.ChangeInspector, InspStates.ChangingInspector)
                .Permit(InspTriggers.Pass, InspStates.ClosablePassed)
                .Permit(InspTriggers.IsPass, InspStates.ClosablePassed)
                .Permit(InspTriggers.Fail, InspStates.SelectingScrapReason)
                .Permit(InspTriggers.IsFail, InspStates.SelectingScrapReason)
                .Permit(InspTriggers.IsFailWithReason, InspStates.ClosableFailed)
                .Permit(InspTriggers.Exit, InspStates.End)
                .Ignore(InspTriggers.SelectScrapReason);

            Configure(InspStates.ChangingInspector)
                .OnExit(CheckSelectedInspectorAction)
                .Permit(InspTriggers.InspectorNotChange, InspStates.Inspecting)
                .Permit(InspTriggers.InspectorChanged, InspStates.UpdatingInspector)
                .Permit(InspTriggers.Pass, InspStates.ClosablePassed)
                .Permit(InspTriggers.Fail, InspStates.SelectingScrapReason)
                .Permit(InspTriggers.Exit, InspStates.End)
                .Ignore(InspTriggers.SelectInspector);

            Configure(InspStates.UpdatingInspector)
                .OnEntry(AdornerTextAction)
                .OnEntry(UpdateAction)
                .Permit(InspTriggers.UpdateSuccessful, InspStates.Inspecting);

            Configure(InspStates.SelectingScrapReason)
                .Permit(InspTriggers.Pass, InspStates.ClosablePassed)
                .Permit(InspTriggers.SelectScrapReason, InspStates.ClosableFailed)
                .Permit(InspTriggers.ChangeInspector, InspStates.ChangingInspector)
                .Permit(InspTriggers.Exit, InspStates.End);

            Configure(InspStates.ClosablePassed)
                .Permit(InspTriggers.CloseInspection, InspStates.Closing)
                .Permit(InspTriggers.Fail, InspStates.SelectingScrapReason)
                .Permit(InspTriggers.Exit, InspStates.End)
                .Permit(InspTriggers.ChangeInspector, InspStates.ChangingInspector)
                .Ignore(InspTriggers.SelectScrapReason)
                .Ignore(InspTriggers.Pass);

            Configure(InspStates.ClosableFailed)
                .Permit(InspTriggers.CloseInspection, InspStates.Closing)
                .Permit(InspTriggers.Pass, InspStates.ClosablePassed)
                .Permit(InspTriggers.ChangeInspector, InspStates.ChangingInspector)
                .Permit(InspTriggers.ChangeScrapReason, InspStates.SelectingScrapReason)
                .Permit(InspTriggers.Exit, InspStates.End)
                .Ignore(InspTriggers.SelectScrapReason);

            Configure(InspStates.Closing)
                .OnEntry(AdornerTextAction)
                .OnEntry(CloseInspectionAction)
                .Permit(InspTriggers.CloseSuccessful, InspStates.End);

            Configure(InspStates.End)
                .Permit(InspTriggers.Restart, InspStates.Start)
                .Ignore(InspTriggers.SelectScrapReason)
                .Ignore(InspTriggers.SelectInspector)
                .OnEntry(ExitAction);
                

          OnTransitioned((t) => 
                              {
                                OnPropertyChanged("State");
                                CommandManager.InvalidateRequerySuggested();
                              } );
      
          //used to debug commands and UI components
          OnTransitioned( (t) => Debug.WriteLine("State Machine transitioned from {0} -> {1} [{2}]", t.Source, t.Destination, t.Trigger ));
        }

        private void OnPropertyChanged(string propertyName)
        {
          if(PropertyChanged != null)
          {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
          }
        }

    }
}
