﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight;

namespace InspectionApp.Messages
{
    class ShowQualityRegisterLogMessage
    {
        public bool RefreshLog { get; set; }

        public bool? IsMissingImage { get; set; }

    }

}
