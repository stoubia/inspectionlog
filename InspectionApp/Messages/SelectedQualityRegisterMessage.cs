﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight;
using Model;

namespace InspectionApp.Messages
{
    class SelectedQualityRegisterMessage
    {
        private QualityRegisterQueue _selectedQualityRegister;
        public QualityRegisterQueue SelectedQualityRegister
        {
            get
            {
                return _selectedQualityRegister;
            }
            set
            {
                _selectedQualityRegister = value;
            }
        }

    }
}
