﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight;
using Model;

namespace InspectionApp.Messages
{
    class UpdateQualityRegisterLogMessage
    {
        

        public UpdateQualityRegisterLogMessage(QualityRegisterQueue qualityRegisterQueue)
        {
            _changedQualityRegisterQueue = qualityRegisterQueue;
        }

        public UpdateQualityRegisterLogMessage(string removedQualityRegisterID)
        {
            _removedQualityRegisterID = removedQualityRegisterID.Trim();
            _removing = true;
        }

        private QualityRegisterQueue _changedQualityRegisterQueue;
        public QualityRegisterQueue ChangedQualityRegisterQueue
        {
            get
            {
                return _changedQualityRegisterQueue;
            }
            set
            {
                _changedQualityRegisterQueue = value;
            }
        }

        private string _removedQualityRegisterID;
        public string RemovedQualityRegisterID
        {
            get
            {
                return _removedQualityRegisterID;
            }
            set
            {
                _removedQualityRegisterID = value;
            }
        }

        private bool _removing;
        public bool Removing
        {
            get
            {
                return _removing;
            }
            set
            {
                _removing = value;
            }
        }

    }

}
