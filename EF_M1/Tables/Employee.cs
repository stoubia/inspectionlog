namespace EF_M1
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
   

    public partial class Employee
    {
        public virtual ICollection<QualityRegister> QualityRegisters {get;set;}

        [Key]
        [Column(Order = 0)]
        [StringLength(10)]
        public string lmeEmployeeID { get; set; }

        
        [Column(Order = 1)]
        [StringLength(50)]
        public string lmeEmployeeName { get; set; }

        
        [Column(Order = 2)]
        [StringLength(5)]
        public string lmeContactTitleID { get; set; }

        
        [Column(Order = 3, TypeName = "text")]
        public string lmeWorkEmailAddress { get; set; }

        public DateTime? lmeHireDate { get; set; }

        public DateTime? lmeTerminationDate { get; set; }

        
        [Column(Order = 4)]
        [StringLength(5)]
        public string lmeTerminationReasonID { get; set; }

        
        [Column(Order = 5, TypeName = "numeric")]
        public decimal lmePayrollEmployee { get; set; }

        
        [Column(Order = 6, TypeName = "numeric")]
        public decimal lmePlannerEmployee { get; set; }

        
        [Column(Order = 7, TypeName = "numeric")]
        public decimal lmeShopEmployee { get; set; }

        
        [Column(Order = 8, TypeName = "numeric")]
        public decimal lmeSupportEmployee { get; set; }

        
        [Column(Order = 9, TypeName = "numeric")]
        public decimal lmeEngineerEmployee { get; set; }

        
        [Column(Order = 10, TypeName = "numeric")]
        public decimal lmeInspectorEmployee { get; set; }

        
        [Column(Order = 11)]
        [StringLength(5)]
        public string lmePlantDepartmentID { get; set; }

        
        [Column(Order = 12)]
        [StringLength(5)]
        public string lmePlantID { get; set; }

        
        [Column(Order = 13)]
        [StringLength(5)]
        public string lmeHomeProductionDepartmentID { get; set; }

        
        [Column(Order = 14)]
        [StringLength(5)]
        public string lmeDefaultWorkCenterID { get; set; }

        
        [Column(Order = 15)]
        [StringLength(5)]
        public string lmeDirectExpenseID { get; set; }

        
        [Column(Order = 16)]
        [StringLength(5)]
        public string lmeIndirectExpenseID { get; set; }

        
        [Column(Order = 17)]
        [StringLength(5)]
        public string lmeCallTypeID { get; set; }

        
        [Column(Order = 18, TypeName = "numeric")]
        public decimal lmeLockShift { get; set; }

        
        [Column(Order = 19, TypeName = "numeric")]
        public decimal lmeDefaultShiftID { get; set; }

        
        [Column(Order = 20)]
        [StringLength(10)]
        public string lmePassword { get; set; }

        
        [Column(Order = 21)]
        [StringLength(10)]
        public string lmeLanguage { get; set; }

        
        [Column(Order = 22, TypeName = "numeric")]
        public decimal lmeSalesEmployee { get; set; }

        
        [Column(Order = 23, TypeName = "numeric")]
        public decimal lmeCommissionRate { get; set; }

        
        [Column(Order = 24, TypeName = "numeric")]
        public decimal lmeEarningType { get; set; }

        
        [Column(Order = 25, TypeName = "numeric")]
        public decimal lmeCanChangePOSPrices { get; set; }

        
        [Column(Order = 26, TypeName = "money")]
        public decimal lmeSOApprovalAmount { get; set; }

        
        [Column(Order = 27, TypeName = "numeric")]
        public decimal lmeQuoterEmployee { get; set; }

        
        [Column(Order = 28, TypeName = "numeric")]
        public decimal lmeBuyerEmployee { get; set; }

        
        [Column(Order = 29, TypeName = "money")]
        public decimal lmePOApprovalAmount { get; set; }

        
        [Column(Order = 30)]
        [StringLength(20)]
        public string lmeUserID { get; set; }

        
        [Column(Order = 31, TypeName = "numeric")]
        public decimal lmeProjectManagerEmployee { get; set; }

        [Column(TypeName = "image")]
        public byte[] lmePicture { get; set; }

        
        [Column(Order = 32, TypeName = "numeric")]
        public decimal lmeWebLoginEnabled { get; set; }

        
        [Column(Order = 33)]
        [StringLength(80)]
        public string lmeWebPassword { get; set; }

        
        [Column(Order = 34)]
        [StringLength(20)]
        public string lmeWebTemplate { get; set; }

        public DateTime? lmeWebExpirationDate { get; set; }

        
        [Column(Order = 35, TypeName = "numeric")]
        public decimal lmeWebTemplateUseM1UserID { get; set; }

        
        [Column(Order = 36, TypeName = "money")]
        public decimal lmeQAApprovalAmount { get; set; }

        
        [Column(Order = 37, TypeName = "numeric")]
        public decimal lmeSortSFEbyWorkcenter { get; set; }

        
        [Column(Order = 38, TypeName = "numeric")]
        public decimal lmeUseEmail { get; set; }

        
        [Column(Order = 39)]
        [StringLength(20)]
        public string lmeCreatedBy { get; set; }

        public DateTime? lmeCreatedDate { get; set; }

        
        [Column(Order = 40)]
        public Guid lmeUniqueID { get; set; }
    }
}
