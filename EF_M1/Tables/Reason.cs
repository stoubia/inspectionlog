namespace EF_M1
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Reason
    {
        
        [Column(Order = 0)]
        [StringLength(5)]
        public string xarReasonID { get; set; }

        [Column(Order = 1)]
        [StringLength(1)]
        public string xarReasonType { get; set; }

        [Column(Order = 2)]
        [StringLength(50)]
        public string xarDescription { get; set; }

        [Column(Order = 3)]
        [StringLength(11)]
        public string xarReasonGLAccountID { get; set; }

        [Column(Order = 4)]
        [StringLength(20)]
        public string xarCreatedBy { get; set; }

        public DateTime? xarCreatedDate { get; set; }

        [Key]
        [Column(Order = 5)]
        public Guid xarUniqueID { get; set; }
    }
}
