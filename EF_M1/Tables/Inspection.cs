namespace EF_M1
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    public partial class Inspection
    {
        
        [Column(Order = 0)]
        [StringLength(10)]
        public string qapInspectionID { get; set; }

        
        //[Column(Order = 1)]
        //[StringLength(5)]
        //public string qapPlantDepartmentID { get; set; }

        
        //[Column(Order = 2)]
        //[StringLength(5)]
        //public string qapPlantID { get; set; }

        
        //[Column(Order = 3)]
        //[StringLength(10)]
        //public string qapProjectID { get; set; }

        
        [Column(Order = 4)]
        [StringLength(10)]
        public string qapInspectorEmployeeID { get; set; }

        public DateTime? qapInspectionDate { get; set; }

        
        [Column(Order = 5)]
        [StringLength(1)]
        public string qapStatus { get; set; }

        public DateTime? qapClosedDate { get; set; }

        
        [Column(Order = 6)]
        [StringLength(20)]
        public string qapCreatedBy { get; set; }

        public DateTime? qapCreatedDate { get; set; }

        [Key]
        [Column(Order = 7)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid qapUniqueID { get; set; }

        public Inspection()
        {
            qapStatus = "O"; //O for Open
            qapCreatedBy = "InspLog";
        }
    }
}
