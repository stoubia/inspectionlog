namespace EF_M1
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    //using System.Data.Entity.Spatial;

    public partial class Job
    {

        public virtual ICollection<JobAssembly> JobAssemblies { get; set; }
        

        [Key]
        [Column(Order = 0)]
        [StringLength(20)]
        public string jmpJobID { get; set; }

        
        [Column(Order = 1)]
        [StringLength(5)]
        public string jmpPlantDepartmentID { get; set; }

        
        [Column(Order = 2)]
        [StringLength(5)]
        public string jmpPlantID { get; set; }

        public DateTime? jmpProductionDueDate { get; set; }

        
        [Column(Order = 3)]
        [StringLength(10)]
        public string jmpCustomerOrganizationID { get; set; }

        public DateTime? jmpJobDate { get; set; }

        
        [Column(Order = 4)]
        [StringLength(30)]
        public string jmpPartID { get; set; }

        
        [Column(Order = 5)]
        [StringLength(15)]
        public string jmpPartRevisionID { get; set; }

        
        [Column(Order = 6)]
        [StringLength(5)]
        public string jmpPartWareHouseLocationID { get; set; }

        
        [Column(Order = 7)]
        [StringLength(15)]
        public string jmpPartBinID { get; set; }

        
        [Column(Order = 8)]
        [StringLength(10)]
        public string jmpCallID { get; set; }

        
        [Column(Order = 9)]
        [StringLength(2)]
        public string jmpUnitOfMeasure { get; set; }

        
        [Column(Order = 10)]
        [StringLength(50)]
        public string jmpPartShortDescription { get; set; }

        
        [Column(Order = 11, TypeName = "text")]
        public string jmpPartLongDescriptionRTF { get; set; }

        
        [Column(Order = 12, TypeName = "text")]
        public string jmpPartLongDescriptionText { get; set; }

        
        [Column(Order = 13, TypeName = "numeric")]
        public decimal jmpOrderQuantity { get; set; }

        
        [Column(Order = 14, TypeName = "numeric")]
        public decimal jmpInventoryQuantity { get; set; }

        
        [Column(Order = 15, TypeName = "numeric")]
        public decimal jmpScrapQuantity { get; set; }

        
        [Column(Order = 16, TypeName = "numeric")]
        public decimal jmpProductionQuantity { get; set; }

        public DateTime? jmpScheduledStartDate { get; set; }

        
        [Column(Order = 17, TypeName = "numeric")]
        public decimal jmpScheduledStartHour { get; set; }

        public DateTime? jmpScheduledDueDate { get; set; }

        
        [Column(Order = 18, TypeName = "numeric")]
        public decimal jmpScheduledDueHour { get; set; }

        
        [Column(Order = 19, TypeName = "numeric")]
        public decimal jmpFirm { get; set; }

        
        [Column(Order = 20, TypeName = "numeric")]
        public decimal jmpJobPriorityID { get; set; }

        
        [Column(Order = 21, TypeName = "numeric")]
        public decimal jmpTimeAndMaterial { get; set; }

        
        [Column(Order = 22, TypeName = "numeric")]
        public decimal jmpPlanningComplete { get; set; }

        
        [Column(Order = 23)]
        [StringLength(10)]
        public string jmpPlannerEmployeeID { get; set; }

        
        [Column(Order = 24, TypeName = "numeric")]
        public decimal jmpScheduleComplete { get; set; }

        
        [Column(Order = 25, TypeName = "numeric")]
        public decimal jmpScheduleLocked { get; set; }

        
        [Column(Order = 26, TypeName = "numeric")]
        public decimal jmpReleasedToFloor { get; set; }

        
        [Column(Order = 27, TypeName = "numeric")]
        public decimal jmpOnHold { get; set; }

        
        [Column(Order = 28, TypeName = "numeric")]
        public decimal jmpReadyToPrint { get; set; }

        
        [Column(Order = 29, TypeName = "numeric")]
        public decimal jmpProductionComplete { get; set; }

        
        [Column(Order = 30, TypeName = "numeric")]
        public decimal jmpQuantityCompleted { get; set; }

        public DateTime? jmpCompletedDate { get; set; }

        
        [Column(Order = 31, TypeName = "numeric")]
        public decimal jmpQuantityShipped { get; set; }

        
        [Column(Order = 32, TypeName = "numeric")]
        public decimal jmpQuantityReceivedToInventory { get; set; }

        
        [Column(Order = 33)]
        [StringLength(10)]
        public string jmpQuoteID { get; set; }

        
        [Column(Order = 34, TypeName = "numeric")]
        public decimal jmpQuoteLineID { get; set; }

        
        [Column(Order = 35)]
        [StringLength(30)]
        public string jmpSourceMethodID { get; set; }

        
        [Column(Order = 36)]
        [StringLength(15)]
        public string jmpSourceRevisionID { get; set; }

        
        [Column(Order = 37)]
        [StringLength(10)]
        public string jmpRMAClaimID { get; set; }

        
        [Column(Order = 38, TypeName = "numeric")]
        public decimal jmpRMAClaimLineID { get; set; }

        
        [Column(Order = 39, TypeName = "numeric")]
        public decimal jmpRMAClaimProblemID { get; set; }

        
        [Column(Order = 40)]
        [StringLength(10)]
        public string jmpProjectID { get; set; }

        
        [Column(Order = 41, TypeName = "text")]
        public string jmpProductionNotesRTF { get; set; }

        
        [Column(Order = 42, TypeName = "text")]
        public string jmpProductionNotesText { get; set; }

        
        [Column(Order = 43, TypeName = "text")]
        public string jmpDocuments { get; set; }

        
        [Column(Order = 44, TypeName = "numeric")]
        public decimal jmpClosed { get; set; }

        public DateTime? jmpClosedDate { get; set; }

        
        [Column(Order = 45)]
        [StringLength(15)]
        public string jmpProjectAreaID { get; set; }

        
        [Column(Order = 46, TypeName = "numeric")]
        public decimal jmpScrapQuantityCompleted { get; set; }

        
        [Column(Order = 47)]
        [StringLength(10)]
        public string jmpShipOrganizationID { get; set; }

        
        [Column(Order = 48)]
        [StringLength(10)]
        public string jmpNonConformanceID { get; set; }

        
        [Column(Order = 49)]
        [StringLength(5)]
        public string jmpShipLocationID { get; set; }

        
        [Column(Order = 50, TypeName = "numeric")]
        public decimal jmpPartForecastYearID { get; set; }

        
        [Column(Order = 51, TypeName = "numeric")]
        public decimal jmpPartForecastPeriodID { get; set; }

        
        [Column(Order = 52)]
        [StringLength(20)]
        public string jmpCreatedBy { get; set; }

        public DateTime? jmpCreatedDate { get; set; }

        
        [Column(Order = 53)]
        public Guid jmpUniqueID { get; set; }

        
        [Column(Order = 54, TypeName = "numeric")]
        public decimal UJMPPLANNINGSTARTED { get; set; }

        [Column(TypeName = "image")]
        public byte[] ujmpPartImage { get; set; }

        
        [Column(Order = 55, TypeName = "numeric")]
        public decimal ujmpHasPartImage { get; set; }

        
        [Column(Order = 56)]
        [StringLength(250)]
        public string ujmpJobImageLocation { get; set; }

        public DateTime? ujmpRecoveryDate { get; set; }

        
        [Column(Order = 57)]
        [StringLength(10)]
        public string ujmpJobRunTypeID { get; set; }

    }
}
