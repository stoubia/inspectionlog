namespace EF_M1
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    public partial class JobOperation
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(20)]
        public string jmoJobID { get; set; }

        
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int jmoJobAssemblyID { get; set; }

        
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int jmoJobOperationID { get; set; }

        
        [Column(Order = 3, TypeName = "numeric")]
        public decimal jmoOperationType { get; set; }

        
        [Column(Order = 4, TypeName = "numeric")]
        public decimal jmoAddedOperation { get; set; }

        
        [Column(Order = 5, TypeName = "numeric")]
        public decimal jmoPrototypeOperation { get; set; }

        
        [Column(Order = 6)]
        [StringLength(5)]
        public string jmoPlantDepartmentID { get; set; }

        
        [Column(Order = 7)]
        [StringLength(5)]
        public string jmoPlantID { get; set; }

        
        [Column(Order = 8)]
        [StringLength(5)]
        public string jmoWorkCenterID { get; set; }

        
        [Column(Order = 9)]
        [StringLength(5)]
        public string jmoProcessID { get; set; }

        
        [Column(Order = 10)]
        [StringLength(50)]
        public string jmoProcessShortDescription { get; set; }

        
        [Column(Order = 11, TypeName = "text")]
        public string jmoProcessLongDescriptionRTF { get; set; }

        
        [Column(Order = 12, TypeName = "text")]
        public string jmoProcessLongDescriptionText { get; set; }

        
        [Column(Order = 13, TypeName = "numeric")]
        public decimal jmoQuantityPerAssembly { get; set; }

        
        [Column(Order = 14, TypeName = "numeric")]
        public decimal jmoSetupHours { get; set; }

        
        [Column(Order = 15, TypeName = "numeric")]
        public decimal jmoProductionStandard { get; set; }

        
        [Column(Order = 16)]
        [StringLength(2)]
        public string jmoStandardFactor { get; set; }

        
        [Column(Order = 17, TypeName = "numeric")]
        public decimal jmoSetupRate { get; set; }

        
        [Column(Order = 18, TypeName = "numeric")]
        public decimal jmoProductionRate { get; set; }

        
        [Column(Order = 19, TypeName = "numeric")]
        public decimal jmoOverheadRate { get; set; }

        
        [Column(Order = 20, TypeName = "numeric")]
        public decimal jmoOperationQuantity { get; set; }

        
        [Column(Order = 21, TypeName = "numeric")]
        public decimal jmoQuantityComplete { get; set; }

        
        [Column(Order = 22, TypeName = "numeric")]
        public decimal jmoSetupPercentComplete { get; set; }

        
        [Column(Order = 23, TypeName = "numeric")]
        public decimal jmoActualSetupHours { get; set; }

        
        [Column(Order = 24, TypeName = "numeric")]
        public decimal jmoActualProductionHours { get; set; }

        
        [Column(Order = 25, TypeName = "numeric")]
        public decimal jmoSetupComplete { get; set; }

        
        [Column(Order = 26, TypeName = "numeric")]
        public decimal jmoProductionComplete { get; set; }

        
        [Column(Order = 27, TypeName = "numeric")]
        public decimal jmoOverlap { get; set; }

        
        [Column(Order = 28, TypeName = "numeric")]
        public decimal jmoMachineType { get; set; }

        
        [Column(Order = 29, TypeName = "numeric")]
        public decimal jmoWorkCenterMachineID { get; set; }

        
        [Column(Order = 30)]
        [StringLength(30)]
        public string jmoPartID { get; set; }

        
        [Column(Order = 31)]
        [StringLength(15)]
        public string jmoPartRevisionID { get; set; }

        
        [Column(Order = 32)]
        [StringLength(2)]
        public string jmoUnitOfMeasure { get; set; }

        
        [Column(Order = 33)]
        [StringLength(10)]
        public string jmoSupplierOrganizationID { get; set; }

        
        [Column(Order = 34)]
        [StringLength(5)]
        public string jmoPurchaseLocationID { get; set; }

        
        [Column(Order = 35, TypeName = "numeric")]
        public decimal jmoFirm { get; set; }

        
        [Column(Order = 36)]
        [StringLength(10)]
        public string jmoPurchaseOrderID { get; set; }

        
        [Column(Order = 37, TypeName = "numeric")]
        public decimal jmoEstimatedUnitCost { get; set; }

        
        [Column(Order = 38, TypeName = "numeric")]
        public decimal jmoMinimumCharge { get; set; }

        
        [Column(Order = 39, TypeName = "numeric")]
        public decimal jmoSetupCharge { get; set; }

        
        [Column(Order = 40, TypeName = "numeric")]
        public decimal jmoCalculatedUnitCost { get; set; }

        
        [Column(Order = 41, TypeName = "numeric")]
        public decimal jmoQuantityBreak1 { get; set; }

        
        [Column(Order = 42, TypeName = "numeric")]
        public decimal jmoUnitCost1 { get; set; }

        
        [Column(Order = 43, TypeName = "numeric")]
        public decimal jmoQuantityBreak2 { get; set; }

        
        [Column(Order = 44, TypeName = "numeric")]
        public decimal jmoUnitCost2 { get; set; }

        
        [Column(Order = 45, TypeName = "numeric")]
        public decimal jmoQuantityBreak3 { get; set; }

        
        [Column(Order = 46, TypeName = "numeric")]
        public decimal jmoUnitCost3 { get; set; }

        
        [Column(Order = 47, TypeName = "numeric")]
        public decimal jmoQuantityBreak4 { get; set; }

        
        [Column(Order = 48, TypeName = "numeric")]
        public decimal jmoUnitCost4 { get; set; }

        
        [Column(Order = 49, TypeName = "numeric")]
        public decimal jmoQuantityBreak5 { get; set; }

        
        [Column(Order = 50, TypeName = "numeric")]
        public decimal jmoUnitCost5 { get; set; }

        
        [Column(Order = 51, TypeName = "numeric")]
        public decimal jmoQuantityBreak6 { get; set; }

        
        [Column(Order = 52, TypeName = "numeric")]
        public decimal jmoUnitCost6 { get; set; }

        
        [Column(Order = 53, TypeName = "numeric")]
        public decimal jmoQuantityBreak7 { get; set; }

        
        [Column(Order = 54, TypeName = "numeric")]
        public decimal jmoUnitCost7 { get; set; }

        
        [Column(Order = 55, TypeName = "numeric")]
        public decimal jmoQuantityBreak8 { get; set; }

        
        [Column(Order = 56, TypeName = "numeric")]
        public decimal jmoUnitCost8 { get; set; }

        
        [Column(Order = 57, TypeName = "numeric")]
        public decimal jmoQuantityBreak9 { get; set; }

        
        [Column(Order = 58, TypeName = "numeric")]
        public decimal jmoUnitCost9 { get; set; }

        public DateTime? jmoStartDate { get; set; }

        public DateTime? jmoDueDate { get; set; }

        
        [Column(Order = 59, TypeName = "numeric")]
        public decimal jmoStartHour { get; set; }

        
        [Column(Order = 60, TypeName = "numeric")]
        public decimal jmoDueHour { get; set; }

        
        [Column(Order = 61, TypeName = "numeric")]
        public decimal jmoEstimatedProductionHours { get; set; }

        
        [Column(Order = 62, TypeName = "numeric")]
        public decimal jmoCompletedSetupHours { get; set; }

        
        [Column(Order = 63, TypeName = "numeric")]
        public decimal jmoCompletedProductionHours { get; set; }

        
        [Column(Order = 64, TypeName = "text")]
        public string jmoDocuments { get; set; }

        
        [Column(Order = 65, TypeName = "text")]
        public string jmoSFEMessageRTF { get; set; }

        
        [Column(Order = 66, TypeName = "text")]
        public string jmoSFEMessageText { get; set; }

        
        [Column(Order = 67, TypeName = "numeric")]
        public decimal jmoClosed { get; set; }

        
        [Column(Order = 68, TypeName = "numeric")]
        public decimal jmoInspectionComplete { get; set; }

        
        [Column(Order = 69, TypeName = "numeric")]
        public decimal jmoInspectionStatus { get; set; }

        
        [Column(Order = 70, TypeName = "numeric")]
        public decimal jmoInspectionType { get; set; }

        
        [Column(Order = 71)]
        [StringLength(10)]
        public string jmoRFQID { get; set; }

        
        [Column(Order = 72, TypeName = "numeric")]
        public decimal jmoMachinesToSchedule { get; set; }

        
        [Column(Order = 73)]
        [StringLength(20)]
        public string jmoCreatedBy { get; set; }

        public DateTime? jmoCreatedDate { get; set; }

        
        [Column(Order = 74)]
        public Guid jmoUniqueID { get; set; }
    }
}
