namespace EF_M1
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    //using System.Data.Entity.Spatial;

    public partial class JobAssembly
    {
        public virtual ICollection<QualityRegister> QualityRegisters { get; set; }

        [Key]
        [Column(Order = 0)]
        [StringLength(20)]
        public string jmaJobID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int jmaJobAssemblyID { get; set; }

       
        [Column(Order = 2, TypeName = "numeric")]
        public decimal jmaLevel { get; set; }

       
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int jmaParentAssemblyID { get; set; }

       
        [Column(Order = 4)]
        [StringLength(30)]
        public string jmaSourceMethodID { get; set; }

       
        [Column(Order = 5)]
        [StringLength(15)]
        public string jmaSourceRevisionID { get; set; }

       
        [Column(Order = 6)]
        [StringLength(30)]
        public string jmaPartID { get; set; }

       
        [Column(Order = 7)]
        [StringLength(15)]
        public string jmaPartRevisionID { get; set; }

       
        [Column(Order = 8)]
        [StringLength(5)]
        public string jmaPartWareHouseLocationID { get; set; }

       
        [Column(Order = 9)]
        [StringLength(15)]
        public string jmaPartBinID { get; set; }

       
        [Column(Order = 10)]
        [StringLength(2)]
        public string jmaUnitOfMeasure { get; set; }

       
        [Column(Order = 11)]
        [StringLength(50)]
        public string jmaPartShortDescription { get; set; }

       
        [Column(Order = 12, TypeName = "text")]
        public string jmaPartLongDescriptionRTF { get; set; }

       
        [Column(Order = 13, TypeName = "text")]
        public string jmaPartLongDescriptionText { get; set; }

       
        [Column(Order = 14, TypeName = "numeric")]
        public decimal jmaQuantityPerParent { get; set; }

       
        [Column(Order = 15, TypeName = "numeric")]
        public decimal jmaEstimatedUnitCost { get; set; }

       
        [Column(Order = 16, TypeName = "numeric")]
        public decimal jmaOrderQuantity { get; set; }

       
        [Column(Order = 17, TypeName = "numeric")]
        public decimal jmaInventoryQuantity { get; set; }

       
        [Column(Order = 18, TypeName = "numeric")]
        public decimal jmaScrapQuantity { get; set; }

       
        [Column(Order = 19, TypeName = "numeric")]
        public decimal jmaScheduledStartHour { get; set; }

       
        [Column(Order = 20, TypeName = "numeric")]
        public decimal jmaQuantityReceivedToInventory { get; set; }

       
        [Column(Order = 21, TypeName = "numeric")]
        public decimal jmaReceivedComplete { get; set; }

       
        [Column(Order = 22, TypeName = "numeric")]
        public decimal jmaScheduledDueHour { get; set; }

       
        [Column(Order = 23, TypeName = "numeric")]
        public decimal jmaProductionQuantity { get; set; }

       
        [Column(Order = 24, TypeName = "numeric")]
        public decimal jmaQuantityToMake { get; set; }

       
        [Column(Order = 25, TypeName = "numeric")]
        public decimal jmaQuantityToPull { get; set; }

       
        [Column(Order = 26, TypeName = "numeric")]
        public decimal jmaQuantityIssued { get; set; }

       
        [Column(Order = 27, TypeName = "numeric")]
        public decimal jmaPullAllFromStock { get; set; }

       
        [Column(Order = 28, TypeName = "numeric")]
        public decimal jmaIssuedComplete { get; set; }

       
        [Column(Order = 29, TypeName = "text")]
        public string jmaProductionNotesRTF { get; set; }

       
        [Column(Order = 30, TypeName = "text")]
        public string jmaProductionNotesText { get; set; }

        public DateTime? jmaScheduledStartDate { get; set; }

        public DateTime? jmaScheduledDueDate { get; set; }

       
        [Column(Order = 31)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int jmaOverlapJobOperationID { get; set; }

       
        [Column(Order = 32, TypeName = "numeric")]
        public decimal jmaOverlapType { get; set; }

       
        [Column(Order = 33, TypeName = "text")]
        public string jmaDocuments { get; set; }

       
        [Column(Order = 34, TypeName = "numeric")]
        public decimal jmaProductionComplete { get; set; }

       
        [Column(Order = 35, TypeName = "numeric")]
        public decimal jmaQuantityCompleted { get; set; }

        public DateTime? jmaCompletedDate { get; set; }

       
        [Column(Order = 36, TypeName = "numeric")]
        public decimal jmaClosed { get; set; }

       
        [Column(Order = 37, TypeName = "numeric")]
        public decimal jmaScrapQuantityCompleted { get; set; }

       
        [Column(Order = 38)]
        [StringLength(20)]
        public string jmaCreatedBy { get; set; }

        public DateTime? jmaCreatedDate { get; set; }

        [Key]
        [Column(Order = 39)]
        public Guid jmaUniqueID { get; set; }

        [Column(TypeName = "image")]
        public byte[] ujmaPartImage { get; set; }

       
        [Column(Order = 40)]
        [StringLength(10)]
        public string ujmaJobRunTypeID { get; set; }

       
        public virtual Job Job { get; set; }
    }
}
