namespace EF_M1
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
   

    public partial class InspectionLine
    {
        
        [Column(Order = 0)]
        [StringLength(10)]
        public string qalInspectionID { get; set; }

        
        [Column(Order = 1, TypeName = "numeric")]
        public decimal qalInspectionLineID { get; set; }

        
        [Column(Order = 2)]
        [StringLength(30)]
        public string qalPartID { get; set; }

        
        [Column(Order = 3)]
        [StringLength(15)]
        public string qalPartRevisionID { get; set; }

        
        //[Column(Order = 4)]
        //[StringLength(5)]
        //public string qalPartWarehouseLocationID { get; set; }

        
        //[Column(Order = 5)]
        //[StringLength(15)]
        //public string qalPartBinID { get; set; }

        
        [Column(Order = 6)]
        [StringLength(50)]
        public string qalPartShortDescription { get; set; }

        
        //[Column(Order = 7)]
        //[StringLength(2)]
        //public string qalUnitOfMeasure { get; set; }

        
        [Column(Order = 8, TypeName = "numeric")]
        public decimal qalQuantityAccepted { get; set; }

        
        [Column(Order = 9, TypeName = "numeric")]
        public decimal qalQuantityRejected { get; set; }

        
        [Column(Order = 10, TypeName = "numeric")]
        public decimal qalQuantityToInspect { get; set; }

        
        [Column(Order = 11, TypeName = "numeric")]
        public decimal qalQuantityToScrap { get; set; }

        
        [Column(Order = 12, TypeName = "numeric")]
        public decimal qalQuantityToReturn { get; set; }

        
        [Column(Order = 13, TypeName = "numeric")]
        public decimal qalActionType { get; set; }

        
        //[Column(Order = 14, TypeName = "numeric")]
        //public decimal qalReturnToSupplier { get; set; }

        
        //[Column(Order = 15)]
        //[StringLength(10)]
        //public string qalSupplierOrganizationID { get; set; }

        
        //[Column(Order = 16)]
        //[StringLength(5)]
        //public string qalPurchaseLocationID { get; set; }

        
        [Column(Order = 17)]
        [StringLength(10)]
        public string qalQualityRegisterID { get; set; }


        [Column(Order = 18)]
        [StringLength(5)]
        public string qalScrapReasonID { get; set; }

        
        [Column(Order = 19, TypeName = "numeric")]
        public decimal qalInspectionComplete { get; set; }

        
        //[Column(Order = 20, TypeName = "numeric")]
        //public decimal qalTransferredToDMR { get; set; }

        
        //[Column(Order = 21, TypeName = "numeric")]
        //public decimal qalPartTransactionID { get; set; }

        
        [Column(Order = 22, TypeName = "numeric")]
        public decimal qalStatus { get; set; }

        //public DateTime? qalApprovalRequestDate { get; set; }

        //public DateTime? qalApprovalDecisionDate { get; set; }

        
        //[Column(Order = 23)]
        //[StringLength(10)]
        //public string qalNextApprovalEmployeeID { get; set; }

        
        //[Column(Order = 24, TypeName = "numeric")]
        //public decimal qalUnitCost { get; set; }

        
        [Column(Order = 25, TypeName = "text")]
        public string qalInspectionNotesText { get; set; }

        
        [Column(Order = 26, TypeName = "text")]
        public string qalInspectionNotesRTF { get; set; }

        
        [Column(Order = 27, TypeName = "numeric")]
        public decimal qalFirstOffInspection { get; set; }

        
        [Column(Order = 28, TypeName = "text")]
        public string qalPartLongDescriptionRTF { get; set; }

        
        [Column(Order = 29, TypeName = "text")]
        public string qalPartLongDescriptionText { get; set; }

        
        //[Column(Order = 30)]
        //[StringLength(10)]
        //public string qalProjectID { get; set; }

        
        //[Column(Order = 31)]
        //[StringLength(15)]
        //public string qalProjectAreaID { get; set; }

        
        [Column(Order = 32)]
        [StringLength(20)]
        public string qalCreatedBy { get; set; }

        public DateTime? qalCreatedDate { get; set; }

        [Key]
        [Column(Order = 33)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid qalUniqueID { get; set; }

        public InspectionLine()
        {
            //qalInspectionID = "";
            //qalInspectionLineID = 1;
            qalPartID = "";
            qalPartRevisionID = "";
            qalPartShortDescription = "";
            qalQuantityAccepted = 0;
            qalQuantityRejected = 0;
            qalQuantityToInspect = 0;
            qalQuantityToScrap = 0;
            qalQuantityToReturn = 0;
            qalActionType = 0;
            qalQualityRegisterID = "";
            qalScrapReasonID = "";
            qalInspectionComplete = 0;
            qalStatus = 3;
            qalInspectionNotesText = "";
            qalInspectionNotesRTF = "";
            qalFirstOffInspection = 0;
            qalPartLongDescriptionRTF = "";
            qalPartLongDescriptionText = "";
            qalCreatedBy = "InspLog";
        }
    }
}
