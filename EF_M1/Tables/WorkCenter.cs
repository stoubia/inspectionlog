namespace EF_M1
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class WorkCenter
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(5)]
        public string xawWorkCenterID { get; set; }

        
        [Column(Order = 1)]
        [StringLength(50)]
        public string xawDescription { get; set; }

        
        [Column(Order = 2)]
        [StringLength(5)]
        public string xawPlantID { get; set; }

        
        [Column(Order = 3)]
        [StringLength(5)]
        public string xawProductionDepartmentID { get; set; }

        
        [Column(Order = 4)]
        [StringLength(5)]
        public string xawProcessID { get; set; }

        
        [Column(Order = 5, TypeName = "numeric")]
        public decimal xawSetupHours { get; set; }

        
        [Column(Order = 6)]
        [StringLength(2)]
        public string xawStandardFactor { get; set; }

        
        [Column(Order = 7, TypeName = "numeric")]
        public decimal xawProductionStandard { get; set; }

        
        [Column(Order = 8, TypeName = "numeric")]
        public decimal xawNumberOfMachines { get; set; }

        
        [Column(Order = 9, TypeName = "numeric")]
        public decimal xawPeoplePerMachine { get; set; }

        
        [Column(Order = 10, TypeName = "numeric")]
        public decimal xawExcludeFromShopLoad { get; set; }

        
        [Column(Order = 11, TypeName = "numeric")]
        public decimal xawHoursMon { get; set; }

        
        [Column(Order = 12, TypeName = "numeric")]
        public decimal xawHoursTue { get; set; }

        
        [Column(Order = 13, TypeName = "numeric")]
        public decimal xawHoursWed { get; set; }

        
        [Column(Order = 14, TypeName = "numeric")]
        public decimal xawHoursThu { get; set; }

        
        [Column(Order = 15, TypeName = "numeric")]
        public decimal xawHoursFri { get; set; }

        
        [Column(Order = 16, TypeName = "numeric")]
        public decimal xawHoursSat { get; set; }

        
        [Column(Order = 17, TypeName = "numeric")]
        public decimal xawHoursSun { get; set; }

        
        [Column(Order = 18, TypeName = "numeric")]
        public decimal xawDayStartTimeMon { get; set; }

        
        [Column(Order = 19, TypeName = "numeric")]
        public decimal xawDayStartTimeTue { get; set; }

        
        [Column(Order = 20, TypeName = "numeric")]
        public decimal xawDayStartTimeWed { get; set; }

        
        [Column(Order = 21, TypeName = "numeric")]
        public decimal xawDayStartTimeThu { get; set; }

        
        [Column(Order = 22, TypeName = "numeric")]
        public decimal xawDayStartTimeFri { get; set; }

        
        [Column(Order = 23, TypeName = "numeric")]
        public decimal xawDayStartTimeSat { get; set; }

        
        [Column(Order = 24, TypeName = "numeric")]
        public decimal xawDayStartTimeSun { get; set; }

        
        [Column(Order = 25, TypeName = "numeric")]
        public decimal xawQueueTime { get; set; }

        
        [Column(Order = 26, TypeName = "numeric")]
        public decimal xawMoveTime { get; set; }

        
        [Column(Order = 27, TypeName = "numeric")]
        public decimal xawOutsideProcessing { get; set; }

        
        [Column(Order = 28, TypeName = "numeric")]
        public decimal xawFiniteTolerance { get; set; }

        
        [Column(Order = 29, TypeName = "numeric")]
        public decimal xawOverheadRate { get; set; }

        
        [Column(Order = 30, TypeName = "numeric")]
        public decimal xawQuotingRate { get; set; }

        
        [Column(Order = 31, TypeName = "numeric")]
        public decimal xawOverheadCalculationType { get; set; }

        
        [Column(Order = 32, TypeName = "numeric")]
        public decimal xawSplitMachineHours { get; set; }

        
        [Column(Order = 33, TypeName = "numeric")]
        public decimal xawSetMachineToLaborHours { get; set; }

        
        [Column(Order = 34, TypeName = "numeric")]
        public decimal xawExportToCalendar { get; set; }

        
        [Column(Order = 35, TypeName = "text")]
        public string xawCalendarLocation { get; set; }

        
        [Column(Order = 36, TypeName = "numeric")]
        public decimal xawCalendarColor { get; set; }

        
        [Column(Order = 37, TypeName = "numeric")]
        public decimal xawStartHour { get; set; }

        
        [Column(Order = 38, TypeName = "numeric")]
        public decimal xawInactive { get; set; }

        public DateTime? xawInactiveDate { get; set; }

        
        [Column(Order = 39)]
        [StringLength(20)]
        public string xawCreatedBy { get; set; }

        public DateTime? xawCreatedDate { get; set; }

        
        [Column(Order = 40)]
        public Guid xawUniqueID { get; set; }
    }
}
