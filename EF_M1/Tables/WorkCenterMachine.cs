namespace EF_M1
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class WorkCenterMachine
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(5)]
        public string xaqWorkCenterID { get; set; }

        
        [Column(Order = 1, TypeName = "numeric")]
        public decimal xaqWorkCenterMachineID { get; set; }

        
        [Column(Order = 2)]
        [StringLength(50)]
        public string xaqDescription { get; set; }

        
        [Column(Order = 3)]
        public Guid xaqUniqueID { get; set; }
    }
}
