namespace EF_M1
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    public partial class QualityRegister
    {

        public virtual JobAssembly JobAssembly { get; set; }

        [ForeignKey("qanOpenedByEmployeeID")]
        public virtual Employee OpenedEmployee { get; set; }

        [ForeignKey("qanAssignedToEmployeeID")]
        public virtual Employee AssignedEmployee { get; set; }

        [Column(Order = 0)]
        [StringLength(10)]
        public string qanQualityRegisterID { get; set; }

        
        //[Column(Order = 1)]
        //[StringLength(5)]
        //public string qanPlantDepartmentID { get; set; }

        
        //[Column(Order = 2)]
        //[StringLength(5)]
        //public string qanPlantID { get; set; }

        
        [Column(Order = 3)]
        [StringLength(10)]
        public string qanCustomerOrganizationID { get; set; }

        
        //[Column(Order = 4)]
        //[StringLength(5)]
        //public string qanShipLocationID { get; set; }

        
        //[Column(Order = 5)]
        //[StringLength(5)]
        //public string qanShipContactID { get; set; }

        
        [Column(Order = 6)]
        [StringLength(30)]
        public string qanPartID { get; set; }

        
        [Column(Order = 7)]
        [StringLength(15)]
        public string qanPartRevisionID { get; set; }

        
        [Column(Order = 8)]
        [StringLength(50)]
        public string qanPartShortDescription { get; set; }

        
        [Column(Order = 9)]
        [StringLength(10)]
        public string qanOpenedByEmployeeID { get; set; }

        
        [Column(Order = 10)]
        [StringLength(10)]
        public string qanAssignedToEmployeeID { get; set; }

        
        [Column(Order = 11)]
        [StringLength(10)]
        public string qanClosedByEmployeeID { get; set; }

        public DateTime? qanAssignedDate { get; set; }

        public DateTime? qanOpenedDate { get; set; }

        public DateTime? qanClosedDate { get; set; }

        
        [Column(Order = 12, TypeName = "numeric")]
        public decimal qanClosed { get; set; }

        
        [Column(Order = 13, TypeName = "text")]
        public string qanLongDescriptionRTF { get; set; }

        
        [Column(Order = 14, TypeName = "text")]
        public string qanLongDescriptionText { get; set; }

        
        //[Column(Order = 15, TypeName = "numeric")]
        //public decimal qanPartTransactionID { get; set; }

        
        [Column(Order = 16, TypeName = "numeric")]
        public decimal qanRegisterQuantity { get; set; }

        
        [Column(Order = 17, TypeName = "numeric")]
        public decimal qanQuantityInspected { get; set; }

        
        //[Column(Order = 18)]
        //[StringLength(10)]
        //public string qanReceiptID { get; set; }

        
        //[Column(Order = 19, TypeName = "numeric")]
        //public decimal qanReceiptLineID { get; set; }

        
        //[Column(Order = 20)]
        //[StringLength(5)]
        //public string qanPartWareHouseLocationID { get; set; }

        
        //[Column(Order = 21)]
        //[StringLength(15)]
        //public string qanPartBinID { get; set; }

        
        //[Column(Order = 22)]
        //[StringLength(2)]
        //public string qanInventoryUnitofMeasure { get; set; }

        
        [Column(Order = 23)]
        [StringLength(20)]
        public string qanJobID { get; set; }

        
        [Column(Order = 24)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int qanJobAssemblyID { get; set; }

        
        //[Column(Order = 25)]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        //public int qanJobMaterialID { get; set; }

        
        [Column(Order = 26)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int qanJobOperationID { get; set; }

        
        //[Column(Order = 27)]
        //[StringLength(10)]
        //public string qanSupplierOrganizationID { get; set; }

        
        //[Column(Order = 28)]
        //[StringLength(5)]
        //public string qanPurchaseLocationID { get; set; }

        
        //[Column(Order = 29)]
        //[StringLength(5)]
        //public string qanPurchaseContactID { get; set; }

        
        //[Column(Order = 30)]
        //[StringLength(10)]
        //public string qanShipmentID { get; set; }

        
        //[Column(Order = 31, TypeName = "numeric")]
        //public decimal qanShipmentLineID { get; set; }

        
        //[Column(Order = 32)]
        //[StringLength(10)]
        //public string qanSalesOrderID { get; set; }

        
        //[Column(Order = 33, TypeName = "numeric")]
        //public decimal qanSalesOrderLineID { get; set; }

        
        //[Column(Order = 34, TypeName = "numeric")]
        //public decimal qanUnitCost { get; set; }

        
        [Column(Order = 35, TypeName = "text")]
        public string qanInspectionNotesText { get; set; }

        
        [Column(Order = 36, TypeName = "text")]
        public string qanInspectionNotesRTF { get; set; }

        
        //[Column(Order = 37)]
        //[StringLength(10)]
        //public string qanPurchaseOrderID { get; set; }

        
        //[Column(Order = 38, TypeName = "numeric")]
        //public decimal qanPurchaseOrderLineID { get; set; }

        
        [Column(Order = 39, TypeName = "numeric")]
        public decimal qanFirstOffInspection { get; set; }

        
        //[Column(Order = 40)]
        //[StringLength(30)]
        //public string qanSerialNumberID { get; set; }

        
        [Column(Order = 41, TypeName = "numeric")]
        public decimal qanSource { get; set; }

        
        //[Column(Order = 42)]
        //[StringLength(10)]
        //public string qanResellerOrganizationID { get; set; }

        
        //[Column(Order = 43)]
        //[StringLength(5)]
        //public string qanResellerLocationID { get; set; }

        
        //[Column(Order = 44)]
        //[StringLength(5)]
        //public string qanResellerContactID { get; set; }

        
        //[Column(Order = 45, TypeName = "numeric")]
        //public decimal qanCreatedFromWeb { get; set; }

        
        //[Column(Order = 46)]
        //[StringLength(10)]
        //public string qanProjectID { get; set; }

        
        [Column(Order = 47, TypeName = "numeric")]
        public decimal qanQualityRegisterType { get; set; }

        
        //[Column(Order = 48, TypeName = "numeric")]
        //public decimal qanSalesOrderDeliveryID { get; set; }

        
        //[Column(Order = 49, TypeName = "numeric")]
        //public decimal qanPulledFromSource { get; set; }

        
        //[Column(Order = 50)]
        //[StringLength(10)]
        //public string qanCallID { get; set; }

        
        [Column(Order = 51, TypeName = "numeric")]
        public decimal qanStatus { get; set; }

        
        //[Column(Order = 52)]
        //[StringLength(15)]
        //public string qanProjectAreaID { get; set; }

        
        //[Column(Order = 53, TypeName = "numeric")]
        //public decimal qanRMAClaimCreated { get; set; }

        
        [Column(Order = 54)]
        [StringLength(20)]
        public string qanCreatedBy { get; set; }

        //[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? qanCreatedDate { get; set; }

        [Key]
        [Column(Order = 55)]
       [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid qanUniqueID { get; set; }

        public QualityRegister()
        {

            qanQualityRegisterID = "";
            // qanPlantDepartmentID = "";
            // qanPlantID = "";
            qanCustomerOrganizationID = "";
            // qanShipLocationID = "";
            // qanShipContactID = "";
            qanPartID = "";
            qanPartRevisionID = "";
            qanPartShortDescription = "";
            qanOpenedByEmployeeID = "";
            qanAssignedToEmployeeID = "";
            qanClosedByEmployeeID = "";
            qanClosed = 0;
            qanLongDescriptionRTF = "";
            qanLongDescriptionText = "";
            //public decimal qanPartTransactionID = "";
            qanRegisterQuantity = 0;
            qanQuantityInspected = 0;
            // qanReceiptID = "";
            //public decimal qanReceiptLineID = "";
            // qanPartWareHouseLocationID = "";
            // qanPartBinID = "";
            // qanInventoryUnitofMeasure = "";
            qanJobID = "";
            qanJobAssemblyID = 0;
            //public int qanJobMaterialID = "";
            qanJobOperationID = 0;
            // qanSupplierOrganizationID = "";
            // qanPurchaseLocationID = "";
            // qanPurchaseContactID = "";
            // qanShipmentID = "";
            //public decimal qanShipmentLineID = "";
            // qanSalesOrderID = "";
            //public decimal qanSalesOrderLineID = "";
            //public decimal qanUnitCost = "";
            qanInspectionNotesText = "";
            qanInspectionNotesRTF = "";
            // qanPurchaseOrderID = "";
            //public decimal qanPurchaseOrderLineID = "";
            qanFirstOffInspection = 0;
            // qanSerialNumberID = "";
            qanSource = 0;
            // qanResellerOrganizationID = "";
            // qanResellerLocationID = "";
            // qanResellerContactID = "";
            //public decimal qanCreatedFromWeb = "";
            // qanProjectID = "";
            qanQualityRegisterType = 0;
            //public decimal qanSalesOrderDeliveryID = "";
            //public decimal qanPulledFromSource = "";
            // qanCallID = "";
            qanStatus = 0;
            // qanProjectAreaID = "";
            //public decimal qanRMAClaimCreated = "";
            qanCreatedBy = "InspLog";


        }

        //public QualityRegister(JobView job, JobOperationDisplay op, EmployeeDisplay emp)
        //{

        //    new QualityRegister();
        //   // NextID nextID = (from a in m1.NextIDs where a.xanTable == "QualityRegisters" select a).First();


        //    //qanQualityRegisterID = (from a in m1.NextIDs where a.xanTable == "QualityRegisters" select a.xanNextID).First().Trim();
        //    qanQualityRegisterID = nextID.xanNextID.Trim();
        //    nextID.xanNextID = (int.Parse(nextID.xanNextID.Trim()) + 1).ToString();

        //    qanCustomerOrganizationID = job.Customer;
        //    qanPartID = job.PartID;
        //    qanPartRevisionID = job.PartRevisionID;
        //    qanPartShortDescription = job.PartShortDescription;
        //    qanOpenedByEmployeeID = emp.EmployeeID;
        //    qanOpenedDate = DateTime.Now;
        //    qanJobID = job.JobID;
        //    qanJobAssemblyID = job.JobAssemblyID;
        //    qanJobOperationID = op.OperationID;
        //    qanSource = 3; //index for Shop Floor Entry I think.
        //    qanCreatedBy = "InspLog";
        //    qanCreatedDate = DateTime.Now;
        //}
    }
}
