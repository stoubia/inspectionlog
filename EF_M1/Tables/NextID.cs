namespace EF_M1
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
   

    public partial class NextID
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(30)]
        public string xanTable { get; set; }

        
        [Column(Order = 1)]
        [StringLength(30)]
        public string xanNextID { get; set; }

        
        [Column(Order = 2, TypeName = "numeric")]
        public decimal xanAutoIncrement { get; set; }

        
        [Column(Order = 3, TypeName = "numeric")]
        public decimal xanIncrementAmount { get; set; }

        
        [Column(Order = 4, TypeName = "numeric")]
        public decimal xanNumericOnly { get; set; }

        
        [Column(Order = 5, TypeName = "numeric")]
        public decimal xanLogChanges { get; set; }

        
        [Column(Order = 6, TypeName = "text")]
        public string xanDatasets { get; set; }

        
        [Column(Order = 7)]
        [StringLength(20)]
        public string xanCreatedBy { get; set; }

        public DateTime? xanCreatedDate { get; set; }

        
        [Column(Order = 8)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid xanUniqueID { get; set; }


        //
        //Now that I think about it, this is probably not necessary since I will
        //never be inserting a new row into this table
        //
        //public NextID()
        //{

        //    xanTable= "";
        //    xanNextID= "";
        //    xanAutoIncrement = 0;
        //    xanIncrementAmount = 0;
        //    xanNumericOnly = 0;
        //    xanLogChanges = 0;
        //    xanDatasets = "";
        //    xanCreatedBy = "";

        //}

        public string GetNextID(string table)
        {
            return "TestNextID";
        }
    }
}
