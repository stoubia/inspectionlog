

namespace EF_M1
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class M1Context : DbContext
    {
        public M1Context()
            : base("M1Production")
        {
        }

        

        public virtual DbSet<JobAssembly> JobAssemblies { get; set; }
        public virtual DbSet<Job> Jobs { get; set; }
        public virtual DbSet<JobOperation> JobOperations { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<NextID> NextIDs { get; set; }
        public virtual DbSet<QualityRegister> QualityRegisters { get; set; }
        public virtual DbSet<WorkCenterMachine> WorkCenterMachines { get; set; }
        public virtual DbSet<WorkCenter> WorkCenters { get; set; }
        public virtual DbSet<InspectionLine> InspectionLines { get; set; }
        public virtual DbSet<Inspection> Inspections { get; set; }
        public virtual DbSet<Reason> Reasons { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new JobConfig());
            modelBuilder.Configurations.Add(new JobAssemblyConfig());
            modelBuilder.Configurations.Add(new JobOperationConfig());
            modelBuilder.Configurations.Add(new EmployeeConfig());
            modelBuilder.Configurations.Add(new NextIDConfig());
            modelBuilder.Configurations.Add(new QualityRegisterConfig());
            modelBuilder.Configurations.Add(new WorkCenterMachineConfig());
            modelBuilder.Configurations.Add(new WorkCenterConfig());
            modelBuilder.Configurations.Add(new InspectionConfig());
            modelBuilder.Configurations.Add(new InspectionLineConfig());
            modelBuilder.Configurations.Add(new ReasonConfig());
           
        }
    }
}
