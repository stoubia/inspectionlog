﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_M1
{
    [Serializable]
    public class M1Exception:Exception
    {
        public M1Exception()
            : base()
        {
        }

        public M1Exception(string message, EntityException ex)
            : base(message, ex)
        {

        }

        public M1Exception(string message, SqlException ex)
            : base(message, ex)
        {

        }

    }
}
