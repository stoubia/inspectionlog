﻿Select qanQualityRegisterID, qanCustomerOrganizationID, qanPartBinID, qanPartRevisionID, qanPartShortDescription
	, qanFirstOffInspection, qanJobID, qanJobAssemblyID, qanJobOperationID
	, qanOpenedByEmployeeID, qanOpenedByEmployeeID, qanAssignedDate, qanAssignedToEmployeeID, qanStatus, qanClosed
from QualityRegisters
order by qanOpenedDate
;

select qapInspectionID, qapInspectorEmployeeID, qapInspectionDate, qapStatus, qapCreatedBy, 
	qalInspectionLineID, qalPartID, qalPartRevisionID, qalQuantityAccepted, qalQuantityRejected, qalQuantityToInspect
	, qalQualityRegisterID, qalInspectionComplete
from Inspections 
	left outer join InspectionLines 
		on qapInspectionID = qalInspectionID
order by qapInspectionDate