﻿using System.Data.Entity.ModelConfiguration;


namespace EF_M1
{
    class QualityRegisterConfig : EntityTypeConfiguration<QualityRegister>
    {
        public QualityRegisterConfig(){

            this.HasRequired(e => e.JobAssembly)
                .WithMany(f => f.QualityRegisters)
                .HasForeignKey(g => new { g.qanJobID, g.qanJobAssemblyID });

           

            this.HasOptional(e => e.OpenedEmployee)
                .WithMany(f => f.QualityRegisters)
                .HasForeignKey(g => g.qanOpenedByEmployeeID);

            //this.HasOptional(e => e.AssignedEmployee)
            //    .WithMany(f => f.QualityRegisters)
            //    .HasForeignKey(g => g.qanAssignedToEmployeeID);

            //auto generated 
            this
                .Property(e => e.qanQualityRegisterID)
                .IsFixedLength()
                .IsUnicode(false);

            //this
            //    .Property(e => e.qanPlantDepartmentID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //this
            //    .Property(e => e.qanPlantID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            this
                .Property(e => e.qanCustomerOrganizationID)
                .IsFixedLength()
                .IsUnicode(false);

            //this
            //    .Property(e => e.qanShipLocationID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //this
            //    .Property(e => e.qanShipContactID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            this
                .Property(e => e.qanPartID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.qanPartRevisionID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.qanPartShortDescription)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.qanOpenedByEmployeeID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.qanAssignedToEmployeeID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.qanClosedByEmployeeID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.qanClosed)
                .HasPrecision(1, 0);

            this
                .Property(e => e.qanLongDescriptionRTF)
                .IsUnicode(false);

            this
                .Property(e => e.qanLongDescriptionText)
                .IsUnicode(false);

            //this
            //    .Property(e => e.qanPartTransactionID)
            //    .HasPrecision(9, 0);

            this
                .Property(e => e.qanRegisterQuantity)
                .HasPrecision(15, 5);

            this
                .Property(e => e.qanQuantityInspected)
                .HasPrecision(15, 5);

            //this
            //    .Property(e => e.qanReceiptID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //this
            //    .Property(e => e.qanReceiptLineID)
            //    .HasPrecision(4, 0);

            //this
            //    .Property(e => e.qanPartWareHouseLocationID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //this
            //    .Property(e => e.qanPartBinID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //this
            //    .Property(e => e.qanInventoryUnitofMeasure)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            this
                .Property(e => e.qanJobID)
                .IsFixedLength()
                .IsUnicode(false);

            //this
            //    .Property(e => e.qanSupplierOrganizationID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //this
            //    .Property(e => e.qanPurchaseLocationID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //this
            //    .Property(e => e.qanPurchaseContactID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //this
            //    .Property(e => e.qanShipmentID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //this
            //    .Property(e => e.qanShipmentLineID)
            //    .HasPrecision(4, 0);

            //this
            //    .Property(e => e.qanSalesOrderID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //this
            //    .Property(e => e.qanSalesOrderLineID)
            //    .HasPrecision(4, 0);

            //this
            //    .Property(e => e.qanUnitCost)
            //    .HasPrecision(15, 5);

            this
                .Property(e => e.qanInspectionNotesText)
                .IsUnicode(false);

            this
                .Property(e => e.qanInspectionNotesRTF)
                .IsUnicode(false);

            //this
            //    .Property(e => e.qanPurchaseOrderID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //this
            //    .Property(e => e.qanPurchaseOrderLineID)
            //    .HasPrecision(4, 0);

            this
                .Property(e => e.qanFirstOffInspection)
                .HasPrecision(1, 0);

            //this
            //    .Property(e => e.qanSerialNumberID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            this
                .Property(e => e.qanSource)
                .HasPrecision(1, 0);

            //this
            //    .Property(e => e.qanResellerOrganizationID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //this
            //    .Property(e => e.qanResellerLocationID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //this
            //    .Property(e => e.qanResellerContactID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //this
            //    .Property(e => e.qanCreatedFromWeb)
            //    .HasPrecision(1, 0);

            //this
            //    .Property(e => e.qanProjectID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            this
                .Property(e => e.qanQualityRegisterType)
                .HasPrecision(1, 0);

            //this
            //    .Property(e => e.qanSalesOrderDeliveryID)
            //    .HasPrecision(4, 0);

            //this
            //    .Property(e => e.qanPulledFromSource)
            //    .HasPrecision(1, 0);

            //this
            //    .Property(e => e.qanCallID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            this
                .Property(e => e.qanStatus)
                .HasPrecision(1, 0);

            //this
            //    .Property(e => e.qanProjectAreaID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //this
            //    .Property(e => e.qanRMAClaimCreated)
            //    .HasPrecision(1, 0);

            this
                .Property(e => e.qanCreatedBy)
                .IsFixedLength()
                .IsUnicode(false);

            //this
            //    .Property(e => e.qanUniqueID);
        }
    }
}
