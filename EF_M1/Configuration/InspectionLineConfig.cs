﻿using System.Data.Entity.ModelConfiguration;


namespace EF_M1
{
    class InspectionLineConfig : EntityTypeConfiguration<InspectionLine>
    {
        public InspectionLineConfig(){

            this
                .Property(e => e.qalInspectionID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.qalInspectionLineID)
                .HasPrecision(4, 0);

            this
                .Property(e => e.qalPartID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.qalPartRevisionID)
                .IsFixedLength()
                .IsUnicode(false);

            //this
            //    .Property(e => e.qalPartWarehouseLocationID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //this
            //    .Property(e => e.qalPartBinID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            this
                .Property(e => e.qalPartShortDescription)
                .IsFixedLength()
                .IsUnicode(false);

            //this
            //    .Property(e => e.qalUnitOfMeasure)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            this
                .Property(e => e.qalQuantityAccepted)
                .HasPrecision(15, 5);

            this
                .Property(e => e.qalQuantityRejected)
                .HasPrecision(15, 5);

            this
                .Property(e => e.qalQuantityToInspect)
                .HasPrecision(15, 5);

            this
                .Property(e => e.qalQuantityToScrap)
                .HasPrecision(15, 5);

            this
                .Property(e => e.qalQuantityToReturn)
                .HasPrecision(15, 5);

            this
                .Property(e => e.qalActionType)
                .HasPrecision(1, 0);

            //this
            //    .Property(e => e.qalReturnToSupplier)
            //    .HasPrecision(1, 0);

            //this
            //    .Property(e => e.qalSupplierOrganizationID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //this
            //    .Property(e => e.qalPurchaseLocationID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            this
                .Property(e => e.qalQualityRegisterID)
                .IsFixedLength()
                .IsUnicode(false);

            //this
            //    .Property(e => e.qalScrapReasonID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            this
                .Property(e => e.qalInspectionComplete)
                .HasPrecision(1, 0);

            //this
            //    .Property(e => e.qalTransferredToDMR)
            //    .HasPrecision(1, 0);

            //this
            //    .Property(e => e.qalPartTransactionID)
            //    .HasPrecision(9, 0);

            this
                .Property(e => e.qalStatus)
                .HasPrecision(1, 0);

            //this
            //    .Property(e => e.qalNextApprovalEmployeeID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //this
            //    .Property(e => e.qalUnitCost)
            //    .HasPrecision(15, 5);

            this
                .Property(e => e.qalInspectionNotesText)
                .IsUnicode(false);

            this
                .Property(e => e.qalInspectionNotesRTF)
                .IsUnicode(false);

            this
                .Property(e => e.qalFirstOffInspection)
                .HasPrecision(1, 0);

            this
                .Property(e => e.qalPartLongDescriptionRTF)
                .IsUnicode(false);

            this
                .Property(e => e.qalPartLongDescriptionText)
                .IsUnicode(false);

            //this
            //    .Property(e => e.qalProjectID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //this
            //    .Property(e => e.qalProjectAreaID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            this
                .Property(e => e.qalCreatedBy)
                .IsFixedLength()
                .IsUnicode(false);
        }
    }
}
