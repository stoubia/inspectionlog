﻿using System.Data.Entity.ModelConfiguration;


namespace EF_M1
{
    class JobConfig : EntityTypeConfiguration<Job>
    {
        public JobConfig()
        {
            
            this.HasKey(e => e.jmpJobID);


            //Auto Generated
            this.Property(e => e.jmpJobID)
                .IsFixedLength()
                .IsUnicode(false);

            this.Property(e => e.jmpPlantDepartmentID)
                .IsFixedLength()
                .IsUnicode(false);

            this.Property(e => e.jmpPlantID)
                .IsFixedLength()
                .IsUnicode(false);

            this.Property(e => e.jmpCustomerOrganizationID)
                .IsFixedLength()
                .IsUnicode(false);

            this.Property(e => e.jmpPartID)
                .IsFixedLength()
                .IsUnicode(false);

            this.Property(e => e.jmpPartRevisionID)
                .IsFixedLength()
                .IsUnicode(false);

           this.Property(e => e.jmpPartWareHouseLocationID)
                .IsFixedLength()
                .IsUnicode(false);

           this.Property(e => e.jmpPartBinID)
                .IsFixedLength()
                .IsUnicode(false);

            this.Property(e => e.jmpCallID)
                .IsFixedLength()
                .IsUnicode(false);

            this.Property(e => e.jmpUnitOfMeasure)
                .IsFixedLength()
                .IsUnicode(false);

            this.Property(e => e.jmpPartShortDescription)
                .IsFixedLength()
                .IsUnicode(false);

            this.Property(e => e.jmpPartLongDescriptionRTF)
                .IsUnicode(false);

            this.Property(e => e.jmpPartLongDescriptionText)
                .IsUnicode(false);

            this.Property(e => e.jmpOrderQuantity)
                .HasPrecision(15, 5);

            this.Property(e => e.jmpInventoryQuantity)
                .HasPrecision(15, 5);

            this.Property(e => e.jmpScrapQuantity)
                .HasPrecision(15, 5);

            this.Property(e => e.jmpProductionQuantity)
                .HasPrecision(15, 5);

            this.Property(e => e.jmpScheduledStartHour)
                .HasPrecision(5, 2);

            this.Property(e => e.jmpScheduledDueHour)
                .HasPrecision(5, 2);

            this.Property(e => e.jmpFirm)
                .HasPrecision(1, 0);

            this.Property(e => e.jmpJobPriorityID)
                .HasPrecision(2, 0);

            this.Property(e => e.jmpTimeAndMaterial)
                .HasPrecision(1, 0);

            this.Property(e => e.jmpPlanningComplete)
                .HasPrecision(1, 0);

            this.Property(e => e.jmpPlannerEmployeeID)
                .IsFixedLength()
                .IsUnicode(false);

            this.Property(e => e.jmpScheduleComplete)
                .HasPrecision(1, 0);

            this.Property(e => e.jmpScheduleLocked)
                .HasPrecision(1, 0);

            this.Property(e => e.jmpReleasedToFloor)
                .HasPrecision(1, 0);

            this.Property(e => e.jmpOnHold)
                .HasPrecision(1, 0);

            this.Property(e => e.jmpReadyToPrint)
                .HasPrecision(1, 0);

            this.Property(e => e.jmpProductionComplete)
                .HasPrecision(1, 0);

            this.Property(e => e.jmpQuantityCompleted)
                .HasPrecision(15, 5);

            this.Property(e => e.jmpQuantityShipped)
                .HasPrecision(15, 5);

            this.Property(e => e.jmpQuantityReceivedToInventory)
                .HasPrecision(15, 5);

            this.Property(e => e.jmpQuoteID)
                .IsFixedLength()
                .IsUnicode(false);

            this.Property(e => e.jmpQuoteLineID)
                .HasPrecision(4, 0);

            this.Property(e => e.jmpSourceMethodID)
                .IsFixedLength()
                .IsUnicode(false);

            this.Property(e => e.jmpSourceRevisionID)
                .IsFixedLength()
                .IsUnicode(false);

            this.Property(e => e.jmpRMAClaimID)
                .IsFixedLength()
                .IsUnicode(false);

            this.Property(e => e.jmpRMAClaimLineID)
                .HasPrecision(4, 0);

            this.Property(e => e.jmpRMAClaimProblemID)
                .HasPrecision(4, 0);

            this.Property(e => e.jmpProjectID)
                .IsFixedLength()
                .IsUnicode(false);

            this.Property(e => e.jmpProductionNotesRTF)
                .IsUnicode(false);

            this.Property(e => e.jmpProductionNotesText)
                .IsUnicode(false);

            this.Property(e => e.jmpDocuments)
                .IsUnicode(false);

            this.Property(e => e.jmpClosed)
                .HasPrecision(1, 0);

            this.Property(e => e.jmpProjectAreaID)
                .IsFixedLength()
                .IsUnicode(false);

            this.Property(e => e.jmpScrapQuantityCompleted)
                .HasPrecision(15, 5);

            this.Property(e => e.jmpShipOrganizationID)
                .IsFixedLength()
                .IsUnicode(false);

            this.Property(e => e.jmpNonConformanceID)
                .IsFixedLength()
                .IsUnicode(false);

            this.Property(e => e.jmpShipLocationID)
                .IsFixedLength()
                .IsUnicode(false);

            this.Property(e => e.jmpPartForecastYearID)
                .HasPrecision(4, 0);

            this.Property(e => e.jmpPartForecastPeriodID)
                .HasPrecision(4, 0);

            this.Property(e => e.jmpCreatedBy)
                .IsFixedLength()
                .IsUnicode(false);

            this.Property(e => e.UJMPPLANNINGSTARTED)
                .HasPrecision(1, 0);

            this.Property(e => e.ujmpHasPartImage)
                .HasPrecision(1, 0);

            this.Property(e => e.ujmpJobImageLocation)
                .IsFixedLength()
                .IsUnicode(false);

            this.Property(e => e.ujmpJobRunTypeID)
                .IsFixedLength()
                .IsUnicode(false);
        }
    }
}
