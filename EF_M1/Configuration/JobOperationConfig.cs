﻿using System.Data.Entity.ModelConfiguration;


namespace EF_M1
{
    class JobOperationConfig : EntityTypeConfiguration<JobOperation>
    {
        public JobOperationConfig()
        {
            this
                .Property(e => e.jmoJobID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.jmoOperationType)
                .HasPrecision(1, 0);

            this
                .Property(e => e.jmoAddedOperation)
                .HasPrecision(1, 0);

            this
                .Property(e => e.jmoPrototypeOperation)
                .HasPrecision(1, 0);

            this
                .Property(e => e.jmoPlantDepartmentID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.jmoPlantID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.jmoWorkCenterID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.jmoProcessID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.jmoProcessShortDescription)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.jmoProcessLongDescriptionRTF)
                .IsUnicode(false);

            this
                .Property(e => e.jmoProcessLongDescriptionText)
                .IsUnicode(false);

            this
                .Property(e => e.jmoQuantityPerAssembly)
                .HasPrecision(13, 6);

            this
                .Property(e => e.jmoSetupHours)
                .HasPrecision(8, 2);

            this
                .Property(e => e.jmoProductionStandard)
                .HasPrecision(10, 4);

            this
                .Property(e => e.jmoStandardFactor)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.jmoSetupRate)
                .HasPrecision(8, 2);

            this
                .Property(e => e.jmoProductionRate)
                .HasPrecision(8, 2);

            this
                .Property(e => e.jmoOverheadRate)
                .HasPrecision(8, 2);

            this
                .Property(e => e.jmoOperationQuantity)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmoQuantityComplete)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmoSetupPercentComplete)
                .HasPrecision(3, 0);

            this
                .Property(e => e.jmoActualSetupHours)
                .HasPrecision(8, 2);

            this
                .Property(e => e.jmoActualProductionHours)
                .HasPrecision(8, 2);

            this
                .Property(e => e.jmoSetupComplete)
                .HasPrecision(1, 0);

            this
                .Property(e => e.jmoProductionComplete)
                .HasPrecision(1, 0);

            this
                .Property(e => e.jmoOverlap)
                .HasPrecision(1, 0);

            this
                .Property(e => e.jmoMachineType)
                .HasPrecision(1, 0);

            this
                .Property(e => e.jmoWorkCenterMachineID)
                .HasPrecision(3, 0);

            this
                .Property(e => e.jmoPartID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.jmoPartRevisionID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.jmoUnitOfMeasure)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.jmoSupplierOrganizationID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.jmoPurchaseLocationID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.jmoFirm)
                .HasPrecision(1, 0);

            this
                .Property(e => e.jmoPurchaseOrderID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.jmoEstimatedUnitCost)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmoMinimumCharge)
                .HasPrecision(8, 2);

            this
                .Property(e => e.jmoSetupCharge)
                .HasPrecision(9, 2);

            this
                .Property(e => e.jmoCalculatedUnitCost)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmoQuantityBreak1)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmoUnitCost1)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmoQuantityBreak2)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmoUnitCost2)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmoQuantityBreak3)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmoUnitCost3)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmoQuantityBreak4)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmoUnitCost4)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmoQuantityBreak5)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmoUnitCost5)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmoQuantityBreak6)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmoUnitCost6)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmoQuantityBreak7)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmoUnitCost7)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmoQuantityBreak8)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmoUnitCost8)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmoQuantityBreak9)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmoUnitCost9)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmoStartHour)
                .HasPrecision(5, 2);

            this
                .Property(e => e.jmoDueHour)
                .HasPrecision(5, 2);

            this
                .Property(e => e.jmoEstimatedProductionHours)
                .HasPrecision(8, 2);

            this
                .Property(e => e.jmoCompletedSetupHours)
                .HasPrecision(8, 2);

            this
                .Property(e => e.jmoCompletedProductionHours)
                .HasPrecision(8, 2);

            this
                .Property(e => e.jmoDocuments)
                .IsUnicode(false);

            this
                .Property(e => e.jmoSFEMessageRTF)
                .IsUnicode(false);

            this
                .Property(e => e.jmoSFEMessageText)
                .IsUnicode(false);

            this
                .Property(e => e.jmoClosed)
                .HasPrecision(1, 0);

            this
                .Property(e => e.jmoInspectionComplete)
                .HasPrecision(1, 0);

            this
                .Property(e => e.jmoInspectionStatus)
                .HasPrecision(1, 0);

            this
                .Property(e => e.jmoInspectionType)
                .HasPrecision(1, 0);

            this
                .Property(e => e.jmoRFQID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.jmoMachinesToSchedule)
                .HasPrecision(3, 0);

            this
                .Property(e => e.jmoCreatedBy)
                .IsFixedLength()
                .IsUnicode(false);
        }
    }
}
