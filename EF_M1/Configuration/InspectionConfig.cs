﻿using System.Data.Entity.ModelConfiguration;


namespace EF_M1
{
    class InspectionConfig : EntityTypeConfiguration<Inspection>
    {
        public InspectionConfig(){
           
            this
                .Property(e => e.qapInspectionID)
                .IsFixedLength()
                .IsUnicode(false);

            //this
            //    .Property(e => e.qapPlantDepartmentID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //this
            //    .Property(e => e.qapPlantID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            //this
            //    .Property(e => e.qapProjectID)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            this
                .Property(e => e.qapInspectorEmployeeID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.qapStatus)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.qapCreatedBy)
                .IsFixedLength()
                .IsUnicode(false);
        }
    }
}
