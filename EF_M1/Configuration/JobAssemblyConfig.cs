﻿using System.Data.Entity.ModelConfiguration;


namespace EF_M1
{
    class JobAssemblyConfig : EntityTypeConfiguration<JobAssembly>
    {
        public JobAssemblyConfig(){

            this
                .HasKey(e => new { e.jmaJobID, e.jmaJobAssemblyID });

            
            this.HasRequired(e => e.Job)
                .WithMany(f => f.JobAssemblies)
                .HasForeignKey(g => g.jmaJobID);

            //Auto Generated
            this
                .Property(e => e.jmaJobID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.jmaLevel)
                .HasPrecision(3, 0);

            this
                .Property(e => e.jmaSourceMethodID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.jmaSourceRevisionID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.jmaPartID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.jmaPartRevisionID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.jmaPartWareHouseLocationID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.jmaPartBinID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.jmaUnitOfMeasure)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.jmaPartShortDescription)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.jmaPartLongDescriptionRTF)
                .IsUnicode(false);

            this
                .Property(e => e.jmaPartLongDescriptionText)
                .IsUnicode(false);

            this
                .Property(e => e.jmaQuantityPerParent)
                .HasPrecision(12, 5);

            this
                .Property(e => e.jmaEstimatedUnitCost)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmaOrderQuantity)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmaInventoryQuantity)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmaScrapQuantity)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmaScheduledStartHour)
                .HasPrecision(5, 2);

            this
                .Property(e => e.jmaQuantityReceivedToInventory)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmaReceivedComplete)
                .HasPrecision(1, 0);

            this
                .Property(e => e.jmaScheduledDueHour)
                .HasPrecision(5, 2);

            this
                .Property(e => e.jmaProductionQuantity)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmaQuantityToMake)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmaQuantityToPull)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmaQuantityIssued)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmaPullAllFromStock)
                .HasPrecision(1, 0);

            this
                .Property(e => e.jmaIssuedComplete)
                .HasPrecision(1, 0);

            this
                .Property(e => e.jmaProductionNotesRTF)
                .IsUnicode(false);

            this
                .Property(e => e.jmaProductionNotesText)
                .IsUnicode(false);

            this
                .Property(e => e.jmaOverlapType)
                .HasPrecision(1, 0);

            this
                .Property(e => e.jmaDocuments)
                .IsUnicode(false);

            this
                .Property(e => e.jmaProductionComplete)
                .HasPrecision(1, 0);

            this
                .Property(e => e.jmaQuantityCompleted)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmaClosed)
                .HasPrecision(1, 0);

            this
                .Property(e => e.jmaScrapQuantityCompleted)
                .HasPrecision(15, 5);

            this
                .Property(e => e.jmaCreatedBy)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.ujmaJobRunTypeID)
                .IsFixedLength()
                .IsUnicode(false);
        }
    }
}
