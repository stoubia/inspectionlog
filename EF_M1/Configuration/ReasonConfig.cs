﻿using System.Data.Entity.ModelConfiguration;


namespace EF_M1
{
    class ReasonConfig : EntityTypeConfiguration<Reason>
    {
        public ReasonConfig()
        {
            this
                .Property(e => e.xarReasonID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.xarReasonType)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.xarDescription)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.xarReasonGLAccountID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.xarCreatedBy)
                .IsFixedLength()
                .IsUnicode(false);
        }
    }
}
