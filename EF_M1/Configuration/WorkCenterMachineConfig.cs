﻿using System.Data.Entity.ModelConfiguration;


namespace EF_M1
{
    class WorkCenterMachineConfig : EntityTypeConfiguration<WorkCenterMachine>
    {
        public WorkCenterMachineConfig(){

            this
                .Property(e => e.xaqWorkCenterID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.xaqWorkCenterMachineID)
                .HasPrecision(3, 0);

            this
                .Property(e => e.xaqDescription)
                .IsFixedLength()
                .IsUnicode(false);
        }
    }
}
