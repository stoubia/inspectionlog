﻿using System.Data.Entity.ModelConfiguration;


namespace EF_M1
{
    class WorkCenterConfig : EntityTypeConfiguration<WorkCenter>
    {
        public WorkCenterConfig(){

            this
                .Property(e => e.xawWorkCenterID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.xawDescription)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.xawPlantID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.xawProductionDepartmentID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.xawProcessID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.xawSetupHours)
                .HasPrecision(8, 2);

            this
                .Property(e => e.xawStandardFactor)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.xawProductionStandard)
                .HasPrecision(10, 4);

            this
                .Property(e => e.xawNumberOfMachines)
                .HasPrecision(3, 0);

            this
                .Property(e => e.xawPeoplePerMachine)
                .HasPrecision(3, 0);

            this
                .Property(e => e.xawExcludeFromShopLoad)
                .HasPrecision(1, 0);

            this
                .Property(e => e.xawHoursMon)
                .HasPrecision(5, 2);

            this
                .Property(e => e.xawHoursTue)
                .HasPrecision(5, 2);

            this
                .Property(e => e.xawHoursWed)
                .HasPrecision(5, 2);

            this
                .Property(e => e.xawHoursThu)
                .HasPrecision(5, 2);

            this
                .Property(e => e.xawHoursFri)
                .HasPrecision(5, 2);

            this
                .Property(e => e.xawHoursSat)
                .HasPrecision(5, 2);

            this
                .Property(e => e.xawHoursSun)
                .HasPrecision(5, 2);

            this
                .Property(e => e.xawDayStartTimeMon)
                .HasPrecision(5, 2);

            this
                .Property(e => e.xawDayStartTimeTue)
                .HasPrecision(5, 2);

            this
                .Property(e => e.xawDayStartTimeWed)
                .HasPrecision(5, 2);

            this
                .Property(e => e.xawDayStartTimeThu)
                .HasPrecision(5, 2);

            this
                .Property(e => e.xawDayStartTimeFri)
                .HasPrecision(5, 2);

            this
                .Property(e => e.xawDayStartTimeSat)
                .HasPrecision(5, 2);

            this
                .Property(e => e.xawDayStartTimeSun)
                .HasPrecision(5, 2);

            this
                .Property(e => e.xawQueueTime)
                .HasPrecision(6, 2);

            this
                .Property(e => e.xawMoveTime)
                .HasPrecision(6, 2);

            this
                .Property(e => e.xawOutsideProcessing)
                .HasPrecision(1, 0);

            this
                .Property(e => e.xawFiniteTolerance)
                .HasPrecision(5, 2);

            this
                .Property(e => e.xawOverheadRate)
                .HasPrecision(8, 2);

            this
                .Property(e => e.xawQuotingRate)
                .HasPrecision(8, 2);

            this
                .Property(e => e.xawOverheadCalculationType)
                .HasPrecision(1, 0);

            this
                .Property(e => e.xawSplitMachineHours)
                .HasPrecision(1, 0);

            this
                .Property(e => e.xawSetMachineToLaborHours)
                .HasPrecision(1, 0);

            this
                .Property(e => e.xawExportToCalendar)
                .HasPrecision(1, 0);

            this
                .Property(e => e.xawCalendarLocation)
                .IsUnicode(false);

            this
                .Property(e => e.xawCalendarColor)
                .HasPrecision(2, 0);

            this
                .Property(e => e.xawStartHour)
                .HasPrecision(5, 2);

            this
                .Property(e => e.xawInactive)
                .HasPrecision(1, 0);

            this
                .Property(e => e.xawCreatedBy)
                .IsFixedLength()
                .IsUnicode(false);
        }
    }
}
