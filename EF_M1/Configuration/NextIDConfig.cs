﻿using System.Data.Entity.ModelConfiguration;


namespace EF_M1
{
    class NextIDConfig : EntityTypeConfiguration<NextID>
    {
        public NextIDConfig(){
            this
                .Property(e => e.xanTable)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.xanNextID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.xanAutoIncrement)
                .HasPrecision(1, 0);

            this
                .Property(e => e.xanIncrementAmount)
                .HasPrecision(3, 0);

            this
                .Property(e => e.xanNumericOnly)
                .HasPrecision(1, 0);

            this
                .Property(e => e.xanLogChanges)
                .HasPrecision(1, 0);

            this
                .Property(e => e.xanDatasets)
                .IsUnicode(false);

            this
                .Property(e => e.xanCreatedBy)
                .IsFixedLength()
                .IsUnicode(false);
        }
    }
}
