﻿using System.Data.Entity.ModelConfiguration;


namespace EF_M1
{
    class EmployeeConfig : EntityTypeConfiguration<Employee>
    {
        public EmployeeConfig(){

            this.HasKey(e => e.lmeEmployeeID);

            //auto generated
            this
                .Property(e => e.lmeEmployeeID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.lmeEmployeeName)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.lmeContactTitleID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.lmeWorkEmailAddress)
                .IsUnicode(false);

            this
                .Property(e => e.lmeTerminationReasonID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.lmePayrollEmployee)
                .HasPrecision(1, 0);

            this
                .Property(e => e.lmePlannerEmployee)
                .HasPrecision(1, 0);

            this
                .Property(e => e.lmeShopEmployee)
                .HasPrecision(1, 0);

            this
                .Property(e => e.lmeSupportEmployee)
                .HasPrecision(1, 0);

            this
                .Property(e => e.lmeEngineerEmployee)
                .HasPrecision(1, 0);

            this
                .Property(e => e.lmeInspectorEmployee)
                .HasPrecision(1, 0);

            this
                .Property(e => e.lmePlantDepartmentID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.lmePlantID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.lmeHomeProductionDepartmentID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.lmeDefaultWorkCenterID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.lmeDirectExpenseID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.lmeIndirectExpenseID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.lmeCallTypeID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.lmeLockShift)
                .HasPrecision(1, 0);

            this
                .Property(e => e.lmeDefaultShiftID)
                .HasPrecision(3, 0);

            this
                .Property(e => e.lmePassword)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.lmeLanguage)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.lmeSalesEmployee)
                .HasPrecision(1, 0);

            this
                .Property(e => e.lmeCommissionRate)
                .HasPrecision(5, 2);

            this
                .Property(e => e.lmeEarningType)
                .HasPrecision(1, 0);

            this
                .Property(e => e.lmeCanChangePOSPrices)
                .HasPrecision(1, 0);

            this
                .Property(e => e.lmeSOApprovalAmount)
                .HasPrecision(19, 4);

            this
                .Property(e => e.lmeQuoterEmployee)
                .HasPrecision(1, 0);

            this
                .Property(e => e.lmeBuyerEmployee)
                .HasPrecision(1, 0);

            this
                .Property(e => e.lmePOApprovalAmount)
                .HasPrecision(19, 4);

            this
                .Property(e => e.lmeUserID)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.lmeProjectManagerEmployee)
                .HasPrecision(1, 0);

            this
                .Property(e => e.lmeWebLoginEnabled)
                .HasPrecision(1, 0);

            this
                .Property(e => e.lmeWebPassword)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.lmeWebTemplate)
                .IsFixedLength()
                .IsUnicode(false);

            this
                .Property(e => e.lmeWebTemplateUseM1UserID)
                .HasPrecision(1, 0);

            this
                .Property(e => e.lmeQAApprovalAmount)
                .HasPrecision(19, 4);

            this
                .Property(e => e.lmeSortSFEbyWorkcenter)
                .HasPrecision(1, 0);

            this
                .Property(e => e.lmeUseEmail)
                .HasPrecision(1, 0);

            this
                .Property(e => e.lmeCreatedBy)
                .IsFixedLength()
                .IsUnicode(false);
        } 
    }
}
