﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logger
{
    

    public class Log
    {
        string logName = Environment.MachineName + "_" + DateTime.Now.ToString("M-d-yyyy") + "_SystemException.log";
        //where I want to save the error log on the network drive
        string networkPath = @"\\TMXSRV03\TMX\Computers and Software\InspectionLog";
        //save the local log on the desktop for easy access viewing woohoo
        string localPath = System.Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);

        public void WriteErrorLog(UnhandledExceptionEventArgs e)
        {
            string strException = string.Format("{0}error。\r\n{1}\r\n\r\n\r\n", DateTime.Now, e.ExceptionObject.ToString());

            try
            {
                File.AppendAllText(Path.Combine(networkPath, logName), strException);
            }
            finally
            {
                //write log locally regardless of network availability
                File.AppendAllText(Path.Combine(localPath, logName), strException);
            }
            
        }

       
    }
}
